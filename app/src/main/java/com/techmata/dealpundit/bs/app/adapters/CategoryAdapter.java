package com.techmata.dealpundit.bs.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.techmata.dealpundit.bs.app.R;

import java.util.ArrayList;

/**
 * Created by Sparks on 26/03/2016.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolderActTime> {

    //contains the list of posts
    //private ArrayList<String> mATLists = new ArrayList<String>();
    private ArrayList<String> myCategories = new ArrayList<String>();
    private LayoutInflater mInflater;
    Context mcontext;
    ViewHolderActTime viewHolder;

    public CategoryAdapter(Context context, ArrayList<String> atLists) {
        this.mcontext = context;
        myCategories = atLists;
        mInflater = LayoutInflater.from(context);
    }

    public void addCategory(int a, String cat) {
        //this.mATLists.add(a);
        //add everything
        myCategories.add(a,cat);
        //update the adapter to reflect the new set of posts
        notifyDataSetChanged();
    }
    public ArrayList<String > getTimes(){
       return myCategories;
    }

    @Override
    public ViewHolderActTime onCreateViewHolder(final ViewGroup parent, int viewType) {
        final View view = mInflater.inflate(R.layout.category_item, parent, false);
        final ViewHolderActTime vHolder = new ViewHolderActTime(view,mcontext);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolderActTime holder, final int position) {

        this.viewHolder = holder;
        holder.tvCategory.setText(myCategories.get(position));
    }

    @Override
    public int getItemCount() {
        return myCategories.size();
    }

    public boolean removeAt(int position) {
            myCategories.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, myCategories.size());
            return true;
    }

    class ViewHolderActTime extends RecyclerView.ViewHolder {

        ImageButton btnClose;
        TextView tvCategory;

        public ViewHolderActTime(final View itemView, final Context mcontext) {
            super(itemView);
            btnClose = (ImageButton) itemView.findViewById(R.id.btnClose);
            btnClose.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setVisibility(View.GONE);
                    removeAt(getAdapterPosition());
                    v.setVisibility(View.VISIBLE);
                }
            });

            tvCategory = (TextView) itemView.findViewById(R.id.tvCategory);

        }
    }
}

