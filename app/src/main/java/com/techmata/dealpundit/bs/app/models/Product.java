package com.techmata.dealpundit.bs.app.models;

import android.util.Base64;

/**
 * Created by Sparks on 15/08/2016.
 */
public class Product {
    int product_id = 0, cat_id = 0;
    long bs_id = 0;
    String product_name = null, product_des = null, imgData = null;
    String company;
    Integer price = 0;

    public Product(String product_name, String product_des, String company, String imgData, Integer price,int cat_id) {
        this.product_name = product_name;
        this.product_des = product_des;
        this.company = company;
        this.imgData = imgData;
        this.price = price;
        this.cat_id = cat_id;
    }
    public Product(int product_id, String product_name, String product_des, String company, String imgData, Integer price,int cat_id) {
        this.product_name = product_name;
        this.product_des = product_des;
        this.company = company;
        this.imgData = imgData;
        this.product_id = product_id;
        this.cat_id = cat_id;
        this.price = price;

    }

    public Product() {
    }



    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_des() {
        return product_des;
    }

    public void setProduct_des(String product_des) {
        this.product_des = product_des;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getImgData() {
        return imgData;
    }
    public byte[] getImg(){
        return Base64.decode(imgData, Base64.DEFAULT);
    }

    public void setImgData(String imgData) {
        this.imgData = imgData;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public int getCat_id() {
        return cat_id;
    }

    public void setCat_id(int cat_id) {
        this.cat_id = cat_id;
    }

    public long getBs_id() {
        return bs_id;
    }

    public void setBs_id(long bs_id) {
        this.bs_id = bs_id;
    }
}
