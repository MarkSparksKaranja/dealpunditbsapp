package com.techmata.dealpundit.bs.app;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.techmata.dealpundit.bs.app.adapters.OffersAdapter;
import com.techmata.dealpundit.bs.app.database.OffersDbHelper;
import com.techmata.dealpundit.bs.app.models.OfferItem;
import com.techmata.dealpundit.bs.app.utils.ServiceCallback;

import java.util.ArrayList;

/**
 * Created by Sparks on 15/08/2016.
 */
public class OffersFragment extends Fragment {

    public static ArrayList<OfferItem> listOffers = new ArrayList<OfferItem>();

    ProgressBar pro_bar;
    public LinearLayoutManager layoutManager;
    private OffersAdapter mPostsAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private OffersDbHelper myDB;
    OfferItem[] offers;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        final View v =  inflater.inflate(R.layout.fragment_home,null);
        pro_bar = (ProgressBar)v.findViewById(R.id.pro_bar);
        pro_bar.setVisibility(View.GONE);

        myDB = new OffersDbHelper(getContext());

        //TODO remove
        //temp code
        if(myDB.getCatCount() == 0){
            myDB.addCategory("Food","");
            myDB.addCategory("Fashion","");
            myDB.addCategory("Electronics","");
            myDB.addCategory("Events","");
            myDB.addCategory("Travel","");
            myDB.addCategory("Others","");
        }
        final TextView mNone = (TextView) v.findViewById(R.id.tvCatNone);
        RecyclerView pagesList = (RecyclerView) v.findViewById(R.id.offersList);
        layoutManager =new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        pagesList.setLayoutManager(layoutManager);

        listOffers.clear();
        offers = myDB.getOffers(null);
        Log.i("myDB", "has Offers "+offers.length);
        for (int c = 0; c < offers.length ; c++){
            listOffers.add(offers[c]);
        }

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeOffers);

        if (listOffers.size() == 0){
            mSwipeRefreshLayout.setRefreshing(true);
        }

        mPostsAdapter = new OffersAdapter(getActivity(), listOffers);
        pagesList.setAdapter(mPostsAdapter);


        myDB.fetchOffers(new ServiceCallback() {
            @Override
            public void onSuccess() {
                //Toast.makeText(getContext(),"Callback",Toast.LENGTH_SHORT).show();
                listOffers.clear();
                offers = myDB.getOffers(null);
                Log.i("myDB", "has Offers "+offers.length);
                for (int c = 0; c < offers.length ; c++){
                    listOffers.add(offers[c]);
                }

                if (listOffers.size() == 0){
                    mNone.setVisibility(View.VISIBLE);
                }else {
                    mNone.setVisibility(View.GONE);
                }
                //tripList.setAdapter(mTripsAdapter);
                mPostsAdapter.setOffers(listOffers);
                Log.i("myCount",String.valueOf(listOffers.size()));
                mSwipeRefreshLayout.setRefreshing(false);
                mSwipeRefreshLayout.setEnabled(true);
            }
        });

        mSwipeRefreshLayout.setEnabled(false);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setEnabled(false);
                Thread thread=  new Thread(){
                    @Override
                    public void run(){
                        myDB.fetchOffers(new ServiceCallback() {
                            @Override
                            public void onSuccess() {
                                listOffers.clear();
                                offers = myDB.getOffers(null);
                                Log.i("myDB", "has Offers "+offers.length);
                                for (int c = 0; c < offers.length ; c++){
                                    listOffers.add(offers[c]);
                                }

                                if (listOffers.size() == 0){
                                    mNone.setVisibility(View.VISIBLE);
                                }else {
                                    mNone.setVisibility(View.GONE);
                                }
                                //tripList.setAdapter(mTripsAdapter);
                                mPostsAdapter.setOffers(listOffers);
                                Log.i("myCount",String.valueOf(listOffers.size()));
                                mSwipeRefreshLayout.setRefreshing(false);
                            }
                        });
                        mSwipeRefreshLayout.setEnabled(true);
                        mSwipeRefreshLayout.setRefreshing(true);
                    }
                };
                thread.start();
            }
        });


       /* if (listOffers.size() == 0){
            mNone.setVisibility(View.VISIBLE);
            return v;
        }else {
            mNone.setVisibility(View.GONE);
        }*/

        //pagesList.setHasFixedSize(true);


        return v;
    }

}

