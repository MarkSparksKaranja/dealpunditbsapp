package com.techmata.dealpundit.bs.app.business;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import com.techmata.dealpundit.bs.app.R;

/**
 * Created by Sparks on 17/08/2016.
 */
public class CreateOfferActivity extends AppCompatActivity {

    Integer MODE = null;
    Long OFFERID = null;
    public static int CREATE_MODE = 0;
    public static int VIEW_MODE = 1;
    public static int EDIT_MODE = 2;

    ActionBar acBar;

    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_offer);

        mFragmentTransaction = mFragmentManager.beginTransaction();
        Bundle bundl = new Bundle();

        MODE = getIntent().getIntExtra("MODE",-1);
        bundl.putInt("MODE", MODE);

        if(MODE == EDIT_MODE || MODE == VIEW_MODE){
            OFFERID = getIntent().getLongExtra("OFFERID",-1);
            bundl.putLong("OFFERID",OFFERID);

        }

        mFragmentManager = getSupportFragmentManager();

        acBar = getSupportActionBar();
        assert acBar != null;


        CreateOfferFragment newFrag =  new CreateOfferFragment();
        newFrag.setArguments(bundl);
        mFragmentTransaction.replace(R.id.containerCO,newFrag).commit();
        // acBar.setTitle("My Offers");
    }

}
