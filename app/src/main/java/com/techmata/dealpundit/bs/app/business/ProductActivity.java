package com.techmata.dealpundit.bs.app.business;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.*;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.techmata.dealpundit.bs.app.R;
import com.techmata.dealpundit.bs.app.database.OffersContract;
import com.techmata.dealpundit.bs.app.database.OffersDbHelper;
import com.techmata.dealpundit.bs.app.models.Product;
import com.techmata.dealpundit.bs.app.utils.MyApplication;
import com.techmata.dealpundit.bs.app.utils.PreferenceHelper;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sparks on 17/08/2016.
 */
public class ProductActivity extends AppCompatActivity {

    ImageView productImage;
    TextView prodName, prodDes, company, prodPrice;
    Product product;
    int pos;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        pos = getIntent().getExtras().getInt("product",-1);
        if (pos != -1 ){
            product = CatalogActivity.mProducts.get(pos);
        }else {
            Integer productID = getIntent().getExtras().getInt("productID",-1);
            //Toast.makeText(OfferActivity.this,"Offer ID -> "+offerID,Toast.LENGTH_SHORT).show();
            if(productID != -1){
                OffersDbHelper mDB = new OffersDbHelper(ProductActivity.this);
                product = mDB.getProduct(productID);
            }else {
               // moveTaskToBack(true);
                finish();
                return;
            }
        }

        productImage = (ImageView) findViewById(R.id.productImage);
        if (product.getImgData() != null){
            //Toast.makeText(BsActivity.this,"Image There",Toast.LENGTH_SHORT).show();
            productImage.setVisibility(View.VISIBLE);
            Glide.with(ProductActivity.this)
                    .load(product.getImg())
                    .asBitmap()
                    .placeholder(R.drawable.image_holder)
                    .error(R.drawable.image_holder)
                    .into(productImage);
        } else {
            //Toast.makeText(BsActivity.this,"Image Null",Toast.LENGTH_SHORT).show();
            productImage.setVisibility(View.GONE);
        }

        prodName = (TextView) findViewById(R.id.productName);
        prodName.setText(product.getProduct_name());

        prodDes = (TextView) findViewById(R.id.productDes);
        prodDes.setText(product.getProduct_des());

        prodPrice = (TextView) findViewById(R.id.productPrice);

        company = (TextView) findViewById(R.id.offerCompany);
        if(product.getCompany() != null){
            company.setText(product.getCompany());
        }else {
            company.setText("Company");
        }

        ActionBar tlb = getSupportActionBar();
        tlb.setDisplayHomeAsUpEnabled(true);
        tlb.setHomeButtonEnabled(true);
        tlb.setTitle(product.getProduct_name());

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_product, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete) {
            deleteProduct(product.getProduct_id(),true);
            if (pos != -1 ){
                CatalogActivity.getInstance().mAdapter.removeAt(pos);
                CatalogActivity.getInstance().tvNone.setVisibility(View.VISIBLE);
            }
            finish();

            return true;
        }else if(id == android.R.id.home){
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean deleteProduct(long productID, boolean api){
        SQLiteDatabase db = new OffersDbHelper(ProductActivity.this).getWritableDatabase();
        db.execSQL("DELETE FROM "+ OffersContract.Products.TABLE_NAME +
                " WHERE " + OffersContract.Products.COLUMN_NAME_PRODUCT_ID+" = "+productID);
        db.close();
        if(api){
            //TODO fix delete product from api
            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.DELETE,
                    MyApplication.URL+"/products/"+String.valueOf(productID),
                    null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if(response != null){
                                Log.i("myRes", response.toString());

                            }else{
                                Toast.makeText(ProductActivity.this,"Error deleting product.",Toast.LENGTH_LONG).show();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("myerr",error.toString());
                    try {
                        String response = new String(error.networkResponse.data,"UTF-8");
                        Log.i("myerrRes",response);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(ProductActivity.this,"Error deleting product.",Toast.LENGTH_LONG).show();
                    error.printStackTrace();
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String,String>();
                    params.put("Content-Type", "application/json");
                    params.put("Accept", "application/json");
                    params.put("Authorisation","Bearer "+ PreferenceHelper.getAccessToken(ProductActivity.this));
                    Log.i("myHeaders", params.toString());
                    return params;
                }
            };
            MyApplication myApp = new MyApplication(ProductActivity.this);
            jsonRequest.setRetryPolicy(new DefaultRetryPolicy());
            MyApplication.getInstance().addToRequestQueue(jsonRequest);

        }
        return true;
    }

}
