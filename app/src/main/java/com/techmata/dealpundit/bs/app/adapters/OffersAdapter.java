package com.techmata.dealpundit.bs.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.techmata.dealpundit.bs.app.OfferActivity;
import com.techmata.dealpundit.bs.app.R;
import com.techmata.dealpundit.bs.app.business.CreateOfferActivity;
import com.techmata.dealpundit.bs.app.models.OfferItem;

import java.util.ArrayList;

/**
 * Created by Sparks on 15/08/2016.
 */
public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.ViewHolderOffers> {

    private final Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<OfferItem> mListOffers = new ArrayList<OfferItem>();
    ViewHolderOffers viewHolder;


    public OffersAdapter(Context context, ArrayList<OfferItem> listPost) {
        this.mContext = context;
        this.mListOffers = listPost;
        mInflater = LayoutInflater.from(context);
    }

    public void setOffers(ArrayList<OfferItem> listOffers) {
        this.mListOffers = listOffers;
        notifyDataSetChanged();
    }

    /*// Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }*/
    @Override
    public OffersAdapter.ViewHolderOffers onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_offer, parent, false);
        ViewHolderOffers viewHolder = new ViewHolderOffers(view,mContext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(OffersAdapter.ViewHolderOffers holder, int position) {
        viewHolder = holder;

        OfferItem currentPost = mListOffers.get(position);

        holder.content.setText(currentPost.getOffer_des());
        holder.title.setText(currentPost.getOffer_name());
        if (currentPost.getImgData() != null){
            holder.offerImage.setVisibility(View.VISIBLE);
            Glide.with(mContext)
                    .load(currentPost.getImg())
                    .asBitmap()
                    .placeholder(R.drawable.image_holder)
                    .error(R.drawable.image_holder)
                    .into(holder.offerImage);
        } else {
            //Toast.makeText(BsActivity.this,"Image Null",Toast.LENGTH_SHORT).show();
            holder.offerImage.setVisibility(View.GONE);
        }
        if (currentPost.getFav()){
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN){
                holder.stats.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.star));
            }else {
                holder.stats.setBackground(ContextCompat.getDrawable(mContext, R.drawable.star_filled));
            }
        }

       /* Glide.with(mContext)
                .load(currentPost.getUserImage())
                .asBitmap()
                .centerCrop()
                .placeholder(R.drawable.image_holder)
                .error(R.drawable.image_holder)
                .into(new BitmapImageViewTarget(holder.userImage) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        holder.userImage.setImageDrawable(circularBitmapDrawable);
                    }
                });*/
    }
    public boolean removeAt(int position) {
        mListOffers.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mListOffers.size());
        return true;
    }

    @Override
    public int getItemCount() {
        return mListOffers.size();
    }
    class ViewHolderOffers extends RecyclerView.ViewHolder {

        ImageView offerImage, userImage;
        TextView content, edit, title, share, stats;

        public ViewHolderOffers (final View itemView, final Context mContext) {
            super(itemView);
            content = (TextView) itemView.findViewById(R.id.content);
            title = (TextView) itemView.findViewById(R.id.title);
            offerImage = (ImageView) itemView.findViewById(R.id.mediaImage);
            share = (TextView) itemView.findViewById(R.id.share);
            stats = (TextView) itemView.findViewById(R.id.stats);
            edit = (TextView) itemView.findViewById(R.id.edit);



            View.OnClickListener openOffer = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(mContext,"image "+String.valueOf(getAdapterPosition()),Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(mContext, OfferActivity.class);
                    intent.putExtra("offer", getAdapterPosition());
                    mContext.startActivity(intent);
                }
            };
            offerImage.setOnClickListener(openOffer);
            title.setOnClickListener(openOffer);
            content.setOnClickListener(openOffer);

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(mContext,"share "+String.valueOf(getAdapterPosition()),Toast.LENGTH_SHORT).show();
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT,mListOffers.get(getAdapterPosition()).getOffer_name() + ":\n" +mListOffers.get(getAdapterPosition()).getOffer_des()
                            + "\n"+ "Shared from Deal Pundit" );
                    sendIntent.setType("text/plain");
                    mContext.startActivity(Intent.createChooser(sendIntent, "Share offer"));
                }
            });

            stats.setOnClickListener(openOffer);

            final PopupMenu popup = new PopupMenu(mContext, edit);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.main_more, popup.getMenu());
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    Intent intent;
                    switch (item.getItemId()){
                        case R.id.action_view:
                            intent = new Intent(mContext, OfferActivity.class);
                            intent.putExtra("offer", getAdapterPosition());
                            mContext.startActivity(intent);
                            break;
                        case R.id.action_edit:
                            removeAt(getAdapterPosition());
                            int offerID = mListOffers.get(getAdapterPosition()).getOffer_id();
                            intent = new Intent(mContext, OfferActivity.class);
                            intent.putExtra("MODE", CreateOfferActivity.EDIT_MODE);
                            intent.putExtra("OFFERID", offerID);
                            //mContext.startActivity(intent);
                            break;
                    }
                    return false;
                }
            });
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int offerID = mListOffers.get(getAdapterPosition()).getOffer_id();
                    Intent intent = new Intent(mContext, OfferActivity.class);
                    intent.putExtra("MODE", CreateOfferActivity.EDIT_MODE);
                    intent.putExtra("OFFERID", offerID);
                    //mContext.startActivity(intent);
                }
            });



        }
    }
}
