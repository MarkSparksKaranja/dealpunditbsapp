package com.techmata.dealpundit.bs.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by Sparks on 15/08/2016.
 */
public class CategoriesFragment extends Fragment {

    private final String[] cat_list = new String[] { "Food", "Electronics","Fashion", "Other" };

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ListView catList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View v =  inflater.inflate(R.layout.fragment_cat,null);

        catList = (ListView) v.findViewById(R.id.listCat);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                inflater.getContext(), android.R.layout.simple_list_item_1,
                cat_list);
        catList.setAdapter(adapter);
        catList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0://Food
                        break;
                    case 1://Electronics
                        break;
                    case 2://Fashion
                        break;
                    case 3://Others
                        break;
                }
                Intent intent = new Intent(getContext(), MainActivity.class);
                intent.putExtra("cat", position);
                startActivity(intent);
            }
        });


        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeCat);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Thread thread=  new Thread(){
                    @Override
                    public void run(){
                        try {
                            synchronized(this){
                                wait(3000);
                            }
                        }
                        catch(InterruptedException ex){
                        }

                        // TODO: refresh categories
                    }
                };
                thread.start();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });




        return v;
    }

}

