package com.techmata.dealpundit.bs.app.business;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.android.volley.*;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.techmata.dealpundit.bs.app.OfferActivity;
import com.techmata.dealpundit.bs.app.R;
import com.techmata.dealpundit.bs.app.adapters.OfferTimesAdapter;
import com.techmata.dealpundit.bs.app.database.OffersDbHelper;
import com.techmata.dealpundit.bs.app.models.Business;
import com.techmata.dealpundit.bs.app.models.OfferItem;
import com.techmata.dealpundit.bs.app.models.OfferTime;
import com.techmata.dealpundit.bs.app.utils.FilePath;
import com.techmata.dealpundit.bs.app.utils.MyApplication;
import com.techmata.dealpundit.bs.app.utils.PreferenceHelper;
import com.theartofdev.edmodo.cropper.CropImage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Sparks on 17/08/2016.
 */
public class CreateOfferFragment extends Fragment {
    private static final int SELECT_PIC = 2020;

    Integer MODE = null;
    public static int CREATE_MODE = 0;
    public static int VIEW_MODE = 1;
    public static int EDIT_MODE = 2;

    String selectedPhotoImagePath = null;
    Uri croppedURI = null;
    OfferTimesAdapter otAdapter;
    OffersDbHelper mDB;
    HashMap<String, Integer> categories;

    ImageView offerImage;
    EditText title, etDescription;
    OfferItem offer;
    Spinner spBs,spCat;
    RecyclerView rvTimes;
    Button btnCreate;
    ProgressBar pgBar;

    SimpleDateFormat sdfDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd",Locale.getDefault());
    SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss",Locale.getDefault());

    private static CreateOfferFragment mInstance;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_create_offer, container, false);
        mInstance =  this;

        try{
            MODE = getArguments().getInt("MODE",CREATE_MODE);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        MODE = CREATE_MODE;

        //MY BUSINESS
        ArrayList<Business> mBusinesses = new OffersDbHelper(getContext()).getMyBusinesses(false);
        final HashMap<String,Long> spBusinesses = new HashMap<String, Long>();
        if(mBusinesses.size() <= 0){
            LinearLayout mNone = (LinearLayout) v.findViewById(R.id.lytNone);
            /*((Button) v.findViewById(R.id.btnRegBs)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), BsActivity.class);
                    intent.putExtra("MODE", BsActivity.ADD_MODE);
                    startActivity(intent);
                }
            });*/
            ((ScrollView)v.findViewById(R.id.scrollCreateOffer)).setVisibility(View.GONE);
            mNone.setVisibility(View.VISIBLE);
            //Toast.makeText(getContext(), "No Business", Toast.LENGTH_SHORT).show();
            return v;
        }
        String[] itemsBs = new String[mBusinesses.size()];
        Integer c = 0;
        for (Business mBS: mBusinesses) {
            spBusinesses.put(mBS.getBsName(),mBS.getId());
            itemsBs[c] = mBS.getBsName();
            c++;
        }
        ArrayAdapter<String> spAdapter = new ArrayAdapter<String>
                (getContext(), android.R.layout.simple_spinner_dropdown_item, itemsBs);
        spBs = (Spinner) v.findViewById(R.id.spBusiness);
        spBs.setAdapter(spAdapter);

        //COMMON
        offerImage = (ImageView) v.findViewById(R.id.offerImage);
        title = (EditText) v.findViewById(R.id.etOfferTitle);
        etDescription = (EditText) v.findViewById(R.id.etOfferDes);
        etDescription.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    OfferDesFragment pDialog  = OfferDesFragment.newInstance();
                    pDialog.show(getChildFragmentManager(),"fragment_offer_des");
                }

            }
        });
        etDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OfferDesFragment pDialog  = OfferDesFragment.newInstance();
                pDialog.show(getChildFragmentManager(),"fragment_offer_des");
            }
        });
        pgBar = (ProgressBar) v.findViewById(R.id.pbCreateOffer);


        //CATEGORIES
        categories = new OffersDbHelper(getContext()).getCategories();
        String[] itemsCats = new String[categories.size()];
        c = 0;
        for (String key : categories.keySet()) {
            itemsCats[c] = key;
            c++;
        }
        ArrayAdapter<String> spCatsAdapter = new ArrayAdapter<String>
                (getContext(), android.R.layout.simple_spinner_dropdown_item, itemsCats);
        spCat = (Spinner) v.findViewById(R.id.spCategory);
        spCat.setAdapter(spCatsAdapter);

        btnCreate = (Button) v.findViewById(R.id.btnCreateOffer);

        rvTimes = (RecyclerView) v.findViewById(R.id.rvTimes);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvTimes.setLayoutManager(layoutManager);

        if (MODE == CREATE_MODE){//CREATE MODE
            offerImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(
                            Intent.createChooser(intent,"Choose Image"),
                            SELECT_PIC);
                }
            });

            //TIMES
            ArrayList<OfferTime> mTimes = new ArrayList<OfferTime>();
            otAdapter = new OfferTimesAdapter(getContext(), mTimes);
            rvTimes.setAdapter(otAdapter);

            Button btnAddTime = (Button) v.findViewById(R.id.btnAddTime);
            btnAddTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    otAdapter.addTime(otAdapter.getItemCount());
                    //Toast.makeText(getContext(),"Added to "+otAdapter.getItemCount(),Toast.LENGTH_SHORT).show();
                }
            });


            //CREATE
            btnCreate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (title.getText().toString().isEmpty()) {
                        title.setError("Title name cannot be empty");
                    }else if (etDescription.getText().toString().isEmpty()) {
                        etDescription.setError("Enter a etDescription");
                    }else {

                        for (OfferTime mOfferTime : otAdapter.getTimes()) {
                            if (mOfferTime.getfDate() == null || mOfferTime.getfTime() == null || mOfferTime.gettDate() == null || mOfferTime.gettTime() == null) {
                                Toast.makeText(getContext(), "Enter all Time details", Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }

                        String imgData = null;
                        if (selectedPhotoImagePath != null){
                            Bitmap selectedImage =  BitmapFactory.decodeFile(selectedPhotoImagePath);
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            selectedImage.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                            byte[] byteArray = stream.toByteArray();
                            imgData = Base64.encodeToString(byteArray, 0);
                            //String filename = selectedPhotoImagePath.substring(selectedPhotoImagePath.lastIndexOf("/")+1);
                        }else {
                            Toast.makeText(getContext(),"Please pick an image",Toast.LENGTH_SHORT).show();
                        }

                        pgBar.setVisibility(View.VISIBLE);

                        btnCreate.setVisibility(View.GONE);
                        mDB = new OffersDbHelper(getContext());
                        Log.i("myTimes",String.valueOf(otAdapter.getTimes().size()));

                        createOffer(new OfferItem(title.getText().toString(),
                                etDescription.getText().toString(),
                                mDB.getMyBusiness(spBusinesses.get(spBs.getSelectedItem()),false),
                                imgData,
                                false,
                                otAdapter.getTimes(),
                                categories.get(spCat.getSelectedItem())));
                    }
                }
            });
        }

        return v;
    }

    public static synchronized CreateOfferFragment getInstance() {
        return mInstance;
    }

    private void createOffer(final OfferItem mOffer){
        JSONObject jsonParam = new JSONObject();

        try {
            jsonParam.put("offer_name",mOffer.getOffer_name());
            jsonParam.put("offer_des",mOffer.getOffer_des());
            jsonParam.put("bs_id", mOffer.getCompany().getId());
            jsonParam.put("cat_id", mOffer.getCat_id());
            jsonParam.put("featured",mOffer.getFav());

            JSONArray times = new JSONArray();
            for (OfferTime offerTime : mOffer.getOfferTimes()) {
                JSONObject timeParams = new JSONObject();
                timeParams.put("start_date",sdfDate.format(offerTime.getfDate().getTime()));
                timeParams.put("start_time",sdfDate.format(offerTime.getfTime().getTime()));
                timeParams.put("stop_date",sdfDate.format(offerTime.gettDate().getTime()));
                timeParams.put("stop_time",sdfTime.format(offerTime.gettTime().getTime()));
                times.put(timeParams);
            }
            //TODO ensure add times is working
            jsonParam.put("offer_times", times);
            jsonParam.put("img_data", mOffer.getImgData());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("myparam",jsonParam.toString());
        // return;
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                MyApplication.URL+"/offers",
                jsonParam,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if(response != null){
                            try {
                                Log.i("myresp",response.toString(2));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (response.has("offer_id")){
                                //Toast.makeText(getContext(),"Ride Requested",Toast.LENGTH_LONG).show();
                                try {
                                    JSONArray offer_times = response.getJSONArray("offer_times");
                                    mOffer.setOffer_id(Integer.parseInt(response.getString("offer_id")));
                                    mDB.addOffer(mOffer,offer_times);

                                    Log.i("mOfferId", String.valueOf(mOffer.getOffer_id()));

                                    //launch bsViewActivity (BsActivity in view mode)
                                    Intent intent = new Intent(getContext(), OfferActivity.class);
                                    intent.putExtra("offerID", mOffer.getOffer_id());
                                    startActivity(intent);
                                    //getActivity().finish();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Log.i("myerr","invalid response, no offer_id");
                                String err = "Error";
                                if (response.has("error")){
                                    try {
                                        err = response.getString("error");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }else{
                            Toast.makeText(getContext(),"Error, please try again.",Toast.LENGTH_LONG).show();
                        }
                        pgBar.setVisibility(View.GONE);
                        btnCreate.setVisibility(View.VISIBLE);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyApplication.getInstance().onErrorResponse(getContext(),error,"Error creating offer.");
                pgBar.setVisibility(View.GONE);
                btnCreate.setVisibility(View.VISIBLE);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();
                params.put("Content-Type", "application/json");
                params.put("Accept", "application/json");
                params.put("Authorisation","Bearer "+ PreferenceHelper.getAccessToken(getContext()));
                Log.i("myHeaders", params.toString());
                return params;
            }
        };
        MyApplication myApp = new MyApplication(getContext());
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy());
        MyApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK) {
                croppedURI = result.getUri();
                //productImage.setImageURI(croppedURI);
                selectedPhotoImagePath = FilePath.getPath(getContext(), croppedURI);

                Log.i("myImagePath", selectedPhotoImagePath);
                if (selectedPhotoImagePath != null) {
                    Glide.with(getContext())
                            .load(new File(selectedPhotoImagePath))
                            .into(offerImage);
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(getContext(), "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
        if (requestCode == SELECT_PIC) {
            if (resultCode == Activity.RESULT_OK) {
                //selectedPhotoImagePath = FileUtils.getPath(getApplicationContext(), data.getData());
               // croppedURI =  Uri.fromFile(new File(getContext().getCacheDir(), "dpAdmin"));
               // Crop.of(data.getData(), croppedURI).asSquare().start(getActivity())
            CropImage.activity(data.getData())
                    .setAspectRatio(1,1)
                    .start(getContext(),this);

            }else {
                Toast.makeText(getContext(),"Error selecting image",Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static class OfferDesFragment extends DialogFragment {

        EditText etPOff, etBuyGet, etBuyGet2, etOther;
        RadioButton rbPOff, rbBuyGet, rbOther;
        TextView tvPOff;
        Button btnSubmit;
        View view;

        String description = "";
        Integer mSelected = 1;


        public OfferDesFragment() {
            // Empty constructor is required for DialogFragment
            // Make sure not to add arguments to the constructor
            // Use `newInstance` instead as shown below
        }

        public static OfferDesFragment newInstance() {
            OfferDesFragment frag = new OfferDesFragment();
            Bundle args = new Bundle();
            frag.setArguments(args);
            return frag;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.dialog_description, container);
        }

        @Override
        public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

            etPOff = (EditText) view.findViewById(R.id.etPOff);
            etPOff.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus)
                        radioCheck(1);
                }
            });

            tvPOff = (TextView) view.findViewById(R.id.tvPOff);
            rbPOff = (RadioButton) view.findViewById(R.id.rbPOff);
            rbPOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        radioCheck(1);
                    }
                }
            });

            etBuyGet = (EditText) view.findViewById(R.id.etBuyGet);
            etBuyGet.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus)
                        radioCheck(2);
                }
            });
            etBuyGet2 = (EditText) view.findViewById(R.id.etBuyGet2);
            etBuyGet.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus)
                        radioCheck(2);
                }
            });
            rbBuyGet = (RadioButton) view.findViewById(R.id.rbBuyGet);
            rbBuyGet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){

                        radioCheck(2);
                    }
                }
            });

            etOther = (EditText) view.findViewById(R.id.etOther);
            etOther.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus)
                        radioCheck(3);
                }
            });

            rbOther = (RadioButton) view.findViewById(R.id.rbOther);
            rbOther.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        radioCheck(3);
                    }
                }
            });

            btnSubmit = (Button) view.findViewById(R.id.btnDescriptionSubmit);
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setDes(mSelected);
                    CreateOfferFragment.getInstance().etDescription.setText(description);
                    dismiss();
                }
            });

            getDialog().setTitle("Add Product");
            // Show soft keyboard automatically and request focus to field
            getDialog().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }

        private void radioCheck(int rb){
            rbPOff.setChecked(false);
            rbBuyGet.setChecked(false);
            rbOther.setChecked(false);
            mSelected = rb;
            switch (rb){
                case 1:
                    rbPOff.setChecked(true);
                    etPOff.requestFocus();
                    break;
                case 2:
                    if (!etBuyGet2.hasFocus()){
                        etBuyGet.requestFocus();
                    }
                    rbBuyGet.setChecked(true);
                    break;
                case 3:
                    rbOther.setChecked(true);
                    etOther.requestFocus();
                    break;
            }
        }

        private void setDes(int rb){
            switch (rb){
                case 1:
                    if (etPOff.getText().toString() != "")
                        description  =  etPOff.getText().toString() + "% off";
                    break;
                case 2:
                    description  =  "Buy " + etBuyGet.getText().toString() +
                            " get "+ etBuyGet2.getText().toString();
                    break;
                case 3:
                    description = etOther.getText().toString();
                    break;
            }
        }


    }
}
