package com.techmata.dealpundit.bs.app.business;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;
import com.android.volley.*;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.techmata.dealpundit.bs.app.R;
import com.techmata.dealpundit.bs.app.adapters.CategoryAdapter;
import com.techmata.dealpundit.bs.app.adapters.ProductsAdapter;
import com.techmata.dealpundit.bs.app.database.OffersDbHelper;
import com.techmata.dealpundit.bs.app.models.Business;
import com.techmata.dealpundit.bs.app.models.Product;
import com.techmata.dealpundit.bs.app.utils.FilePath;
import com.techmata.dealpundit.bs.app.utils.MyApplication;
import com.techmata.dealpundit.bs.app.utils.PreferenceHelper;
import com.techmata.dealpundit.bs.app.utils.ServiceCallback;
import com.theartofdev.edmodo.cropper.CropImage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sparks on 16/11/2016.
 */

public class CatalogActivity extends AppCompatActivity {
    static CatalogActivity mInstance;

    public long bsID;
    private Boolean isFabOpen = false;
    private FloatingActionButton fab,fab1,fab2;
    private ProgressBar pbCatalog;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;

    public static ArrayList<Product> mProducts;
    ProductsAdapter mAdapter;
    TextView tvNone;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalog);

        mInstance = this;


        bsID = getIntent().getLongExtra("bsID",-1);
        //Toast.makeText(BsActivity.this,"mybsID -> "+bsID,Toast.LENGTH_SHORT).show();
        if(bsID <= 0) {
            Toast.makeText(CatalogActivity.this,"Product info not found",Toast.LENGTH_SHORT).show();
            return;
        }

        pbCatalog = (ProgressBar) findViewById(R.id.pbCatalog);

        final OffersDbHelper mDB = new OffersDbHelper(CatalogActivity.this);
        Business mBusiness = mDB.getMyBusiness(bsID, false);

        mProducts = mDB.getMyBusinessCatalog(bsID);
        Log.i("myCatalog",String.valueOf(mProducts.size()));

        RecyclerView productsList = (RecyclerView) findViewById(R.id.catalogList);
        GridLayoutManager layoutManager =new GridLayoutManager(CatalogActivity.this,2);
        //layoutManager.setOrientation(GridLayoutManager.);
        productsList.setLayoutManager(layoutManager);
        mAdapter = new ProductsAdapter(CatalogActivity.this,mProducts);
        productsList.setAdapter(mAdapter);
        tvNone = (TextView) findViewById(R.id.tvNone);

        if(mProducts.size() <= 0 ){
            pbCatalog.setVisibility(View.VISIBLE);
            mDB.fetchCatalog(bsID,new ServiceCallback() {
                @Override
                public void onSuccess() {
                    pbCatalog.setVisibility(View.GONE);
                    if(mProducts.size() <= 0){
                        tvNone.setVisibility(View.VISIBLE);
                    }else {
                        tvNone.setVisibility(View.GONE);
                        mProducts.clear();
                        mProducts = mDB.getMyBusinessCatalog(bsID);
                    }

                    mAdapter.setProducts(mProducts);
                   /* mSwipeRefreshLayout.setRefreshing(false);
                    mSwipeRefreshLayout.setEnabled(true);*/
                }
            });
        }






        fab = (FloatingActionButton) findViewById(R.id.fabAddCatalog);
        fab1 = (FloatingActionButton)findViewById(R.id.addProduct);
        fab2 = (FloatingActionButton) findViewById(R.id.addCategory);
        fab_open = AnimationUtils.loadAnimation(CatalogActivity.this, R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(CatalogActivity.this,R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(CatalogActivity.this,R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(CatalogActivity.this,R.anim.rotate_backward);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateFAB();
            }
        });
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Add Product Dialog
                ProductDialogFragment pDialog  = ProductDialogFragment.newInstance(1,bsID);
                pDialog.show(getSupportFragmentManager(),"fragment_add_product");
            }
        });
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Add Category Dialog
                CategoryDialogFragment cDialog  = CategoryDialogFragment.newInstance(1,bsID);
                cDialog.show(getSupportFragmentManager(),"fragment_edit_categories");
            }
        });

    }
    public static synchronized CatalogActivity getInstance() {
        return mInstance;
    }

    public void animateFAB(){
        //Toast.makeText(CatalogActivity.this, "Animate FAB", Toast.LENGTH_SHORT).show();
        if(isFabOpen){
            fab.startAnimation(rotate_backward);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
            fab1.setClickable(false);
            fab2.setClickable(false);
            isFabOpen = false;

        } else {

            fab.startAnimation(rotate_forward);
            fab1.startAnimation(fab_open);
            fab2.startAnimation(fab_open);
            fab1.setClickable(true);
            fab2.setClickable(true);
            isFabOpen = true;

        }
    }
    public static class ProductDialogFragment extends DialogFragment {

        private EditText etName, etPrice, etDesc;
        private Button btnSubmit;
        private ImageView ivProd;

        HashMap<String, Integer> categories;

        private Spinner spCat;
        private ProgressBar pgBar;

        OffersDbHelper mDB;

        Long bsID;
        Integer SELECT_PIC = 2021;
        String selectedPhotoImagePath = null;

        public ProductDialogFragment() {
            // Empty constructor is required for DialogFragment
            // Make sure not to add arguments to the constructor
            // Use `newInstance` instead as shown below
        }

        public static ProductDialogFragment newInstance(Integer MODE,Long bsID) {
            ProductDialogFragment frag = new ProductDialogFragment();
            Bundle args = new Bundle();
            args.putInt("MODE",MODE);
            args.putLong("bsID",bsID);
            frag.setArguments(args);
            return frag;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.dialog_product, container);
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

            mDB  =  new  OffersDbHelper(getContext());

            Integer MODE = getArguments().getInt("MODE",0);
            bsID = getArguments().getLong("bsID",0);

            categories = new OffersDbHelper(getContext()).getBsCategories(bsID);
            if(categories.size() == 0){
                Toast.makeText(getContext(), "Please create categories first", Toast.LENGTH_SHORT).show();
                dismiss();
            }
            Log.i("myCategories",categories.toString());
            String[] itemsCats = new String[categories.size()];
            int c = 0;
            for (String key : categories.keySet()) {
                itemsCats[c] = key;
                c++;
            }
            ArrayAdapter<String> spCatsAdapter = new ArrayAdapter<String>
                    (getContext(), android.R.layout.simple_spinner_dropdown_item, itemsCats);
            spCat = (Spinner) view.findViewById(R.id.spCategory);
            spCat.setAdapter(spCatsAdapter);

            etName = (EditText) view.findViewById(R.id.etProdName);
            etDesc = (EditText) view.findViewById(R.id.etProdDesc);
            etPrice = (EditText) view.findViewById(R.id.etProdPrice);

            ivProd = (ImageView) view.findViewById(R.id.ivProd);
            ivProd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(
                            Intent.createChooser(intent,"Choose Image"),
                            SELECT_PIC);
                }
            });

            pgBar = (ProgressBar) view.findViewById(R.id.pbProduct);

            btnSubmit = (Button) view.findViewById(R.id.btnProdSubmit);
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (etName.getText().toString().isEmpty()) {
                        etName.setError("Name cannot be empty");
                    }else if (etDesc.getText().toString().isEmpty()) {
                        etDesc.setError("Enter a etDescription");
                    }else if (etPrice.getText().toString().isEmpty()) {
                        etPrice.setError("Enter product price");
                    }else {
                        String imgData = null;
                        if (selectedPhotoImagePath != null){
                            Bitmap selectedImage =  BitmapFactory.decodeFile(selectedPhotoImagePath);
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            selectedImage.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                            byte[] byteArray = stream.toByteArray();
                            imgData = Base64.encodeToString(byteArray, 0);
                        }else {
                            Toast.makeText(getContext(),"Please pick an image",Toast.LENGTH_SHORT).show();
                        }

                        pgBar.setVisibility(View.VISIBLE);
                        btnSubmit.setVisibility(View.GONE);

                        OffersDbHelper mDB = new OffersDbHelper(getContext());

                        Product mProduct = new Product(etName.getText().toString(),
                                etDesc.getText().toString(),
                                null,
                                imgData,
                                Integer.valueOf(etPrice.getText().toString()),
                                categories.get(spCat.getSelectedItem())
                        );
                        mProduct.setBs_id(bsID);
                        createProduct(mProduct);



                    }
                    //dismiss();
                }
            });

            getDialog().setTitle("Add Product");
            // Show soft keyboard automatically and request focus to field
            getDialog().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == Activity.RESULT_OK) {
                    Uri croppedURI = result.getUri();
                    //productImage.setImageURI(croppedURI);
                    selectedPhotoImagePath = FilePath.getPath(getContext(), croppedURI);

                    Log.i("myImagePath", selectedPhotoImagePath);
                    if (selectedPhotoImagePath != null) {
                        Glide.with(getContext())
                                .load(new File(selectedPhotoImagePath))
                                .into(ivProd);
                    }
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Toast.makeText(getContext(), "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
                }
            }
            if (requestCode == SELECT_PIC) {
                if (resultCode == Activity.RESULT_OK) {
                    //selectedPhotoImagePath = FileUtils.getPath(getApplicationContext(), data.getData());
                    // croppedURI =  Uri.fromFile(new File(getContext().getCacheDir(), "dpAdmin"));
                    // Crop.of(data.getData(), croppedURI).asSquare().start(getActivity())
                    CropImage.activity(data.getData())
                            .setAspectRatio(1,1)
                            .start(getContext(),this);

                }else {
                    Toast.makeText(getContext(),"Error selecting image",Toast.LENGTH_SHORT).show();
                }
            }
        }
        private void createProduct(final Product mProduct){
            JSONObject jsonParam = new JSONObject();

            try {
                jsonParam.put("prod_name",mProduct.getProduct_name());
                jsonParam.put("prod_des",mProduct.getProduct_des());
                jsonParam.put("price",mProduct.getPrice());
                jsonParam.put("bs_id", bsID);
                jsonParam.put("cat_id", mProduct.getCat_id());
                jsonParam.put("img_data", mProduct.getImgData());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.i("myparam",jsonParam.toString());
            // return;
            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                    MyApplication.URL+"/products",
                    jsonParam,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if(response != null){
                                try {
                                    Log.i("myresp",response.toString(2));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                if (response.has("product_id")){
                                    //Toast.makeText(getContext(),"Ride Requested",Toast.LENGTH_LONG).show();
                                    try {
                                        mProduct.setProduct_id(Integer.parseInt(response.getString("product_id")));
                                        mDB.addProduct(mProduct);
                                        if(mProducts.size() <= 0){
                                            CatalogActivity.getInstance().tvNone.setVisibility(View.GONE);
                                        }
                                        mProducts.add(mProduct);
                                        //CatalogActivity.getInstance().mAdapter.setProducts(mDB.getMyBusinessCatalog(bsID, true));
                                        CatalogActivity.getInstance().mAdapter.setProducts(mProducts);
                                        dismiss();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Log.i("myerr","invalid response, no offer_id");
                                    String err = "Error";
                                    if (response.has("error")){
                                        try {
                                            err = response.getString("error");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }else{
                                Toast.makeText(getContext(),"Error, please try again.",Toast.LENGTH_LONG).show();
                            }
                            pgBar.setVisibility(View.GONE);
                            btnSubmit.setVisibility(View.VISIBLE);

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    MyApplication.getInstance().onErrorResponse(getContext(),error,"Error creating product.");
                    pgBar.setVisibility(View.GONE);
                    btnSubmit.setVisibility(View.VISIBLE);
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String,String>();
                    params.put("Content-Type", "application/json");
                    params.put("Accept", "application/json");
                    params.put("Authorisation","Bearer "+ PreferenceHelper.getAccessToken(getContext()));
                    Log.i("myHeaders", params.toString());
                    return params;
                }
            };
            MyApplication myApp = new MyApplication(getContext());
            jsonRequest.setRetryPolicy(new DefaultRetryPolicy());
            MyApplication.getInstance().addToRequestQueue(jsonRequest);
        }
    }

    public static class CategoryDialogFragment extends DialogFragment {
        private EditText etCat;
        private RecyclerView rvCat;
        private Button btnSubmit,btAddCat;
        private long bsID;
        private  ProgressBar pgBar;

        OffersDbHelper mDB;

        ArrayList<String> nCategories;

        private CategoryAdapter mAdapter;
        String selectedPhotoImagePath = null;

        public CategoryDialogFragment() {
            // Empty constructor is required for DialogFragment
            // Make sure not to add arguments to the constructor
            // Use `newInstance` instead as shown below
        }

        public static CategoryDialogFragment newInstance(Integer MODE,Long bsID) {
            CategoryDialogFragment frag = new CategoryDialogFragment();
            Bundle args = new Bundle();
            args.putInt("MODE",MODE);
            args.putLong("bsID",bsID);
            frag.setArguments(args);
            return frag;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.dialog_categories, container);
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

            mDB  =  new  OffersDbHelper(getContext());

           // Integer MODE = getArguments().getInt("MODE",0);
            bsID = getArguments().getLong("bsID",0);

            rvCat = (RecyclerView) view.findViewById(R.id.rvCategories);
            LinearLayoutManager layoutManager =new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvCat.setLayoutManager(layoutManager);
            final ArrayList<String> categories = new ArrayList<String>();
            //Add existing categories
            HashMap<String, Integer> cats = mDB.getBsCategories(bsID);
            for (String cat:
                 cats.keySet()) {
                categories.add(cat);
            }
            nCategories = new ArrayList<String>();
            mAdapter = new CategoryAdapter(getActivity(),categories);
            rvCat.setAdapter(mAdapter);
            etCat = (EditText) view.findViewById(R.id.etCat);

            btAddCat = (Button) view.findViewById(R.id.btnAddCat);
            btAddCat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String category = etCat.getText().toString();
                    if(!category.isEmpty()){
                        mAdapter.addCategory(mAdapter.getItemCount(),category);
                        //categories.add(categories.size(),category);
                        nCategories.add(category);
                        etCat.setText("");
                    }else{
                        Toast.makeText(getActivity(), "Invalid category", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            btnSubmit = (Button) view.findViewById(R.id.btnCategoriesSubmit);
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnSubmit.setVisibility(View.GONE);
                    saveCategories(bsID,nCategories);
                    dismiss();
                }
            });

            pgBar = (ProgressBar) view.findViewById(R.id.pbCategories);

            getDialog().setTitle("Edit Categories");
            // Show soft keyboard automatically and request focus to field
            getDialog().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }

        private void saveCategories(final long bsID, final ArrayList<String> mCategories){
            final JSONArray categories = new JSONArray();
            try {
                for (String category : mCategories) {
                    JSONObject cat = new JSONObject();
                    cat.put("bs_id",bsID);
                    cat.put("cat_name",category);
                    cat.put("cat_des",category);
                    categories.put(cat);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.i("myparam",categories.toString());
            // return;
            JsonArrayRequest jsonRequest = new JsonArrayRequest(Request.Method.POST,
                    MyApplication.URL+"/businesses/category",
                    categories,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            if(response != null){
                                Log.i("myRes",response.toString());
                                //OffersDbHelper mDB = new OffersDbHelper(getContext());
                                for (int c  = 0; c < response.length(); c++){
                                    try {
                                        JSONObject category = response.getJSONObject(c);
                                        if (!category.toString().contains("error")){

                                            Integer bs_id = category.getInt("bs_id");
                                            Integer catID = category.getInt("cat_id");
                                            String name = category.getString("cat_name");
                                            String des = category.getString("cat_des");

                                            mDB.addBsCategory(bs_id,catID,name,des);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                dismiss();

                            }else{
                                Toast.makeText(getContext(),"Error saving categories.",Toast.LENGTH_LONG).show();
                            }
                            pgBar.setVisibility(View.GONE);
                            btnSubmit.setVisibility(View.VISIBLE);

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    MyApplication.getInstance().onErrorResponse(getContext(),error,"Error updating categories.");
                    pgBar.setVisibility(View.GONE);
                    btnSubmit.setVisibility(View.VISIBLE);
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String,String>();
                    params.put("Content-Type", "application/json");
                    params.put("Accept", "application/json");
                    params.put("Authorisation","Bearer "+ PreferenceHelper.getAccessToken(getContext()));
                    Log.i("myHeaders", params.toString());
                    return params;
                }
            };
            MyApplication myApp = new MyApplication(getContext());
            jsonRequest.setRetryPolicy(new DefaultRetryPolicy());
            MyApplication.getInstance().addToRequestQueue(jsonRequest);
        }
    }
}
