package com.techmata.dealpundit.bs.app;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.techmata.dealpundit.bs.app.adapters.OffersAdapter;
import com.techmata.dealpundit.bs.app.database.OffersDbHelper;
import com.techmata.dealpundit.bs.app.models.OfferItem;

import java.util.ArrayList;

/**
 * Created by Sparks on 15/08/2016.
 */
public class FavFragment extends Fragment {

    public static ArrayList<OfferItem> listFavs = new ArrayList<OfferItem>();

    public LinearLayoutManager layoutManager;
    private OffersAdapter mOffersAdapter;

    private OffersDbHelper myDB;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View v =  inflater.inflate(R.layout.fragment_fav,null);

        myDB = new OffersDbHelper(getContext());

        listFavs.clear();
        OfferItem[] offers = myDB.getOffers(null);
        for (int c = 0; c < offers.length ; c++){
            if(offers[c].getFav()){
                listFavs.add(offers[c]);
            }
        }

        TextView mNone = (TextView) v.findViewById(R.id.tvNone);
        if (listFavs.size() == 0) {
            mNone.setVisibility(View.VISIBLE);
            return v;
        }

        final RecyclerView pagesList = (RecyclerView) v.findViewById(R.id.favList);
        pagesList.setHasFixedSize(true);
        layoutManager =new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        pagesList.setLayoutManager(layoutManager);

        mOffersAdapter = new OffersAdapter(getActivity(), listFavs);
        pagesList.setAdapter(mOffersAdapter);
        Log.i("myFavO", "Adapter has Offers "+mOffersAdapter.getItemCount());


        /*mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeFav);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Thread thread=  new Thread(){
                    @Override
                    public void run(){
                        listFavs.clear();
                        OfferItem[] offers = myDB.getOffers(null);
                        for (int c = 0; c < offers.length ; c++){
                            if(offers[c].getPrice()){
                                listFavs.add(offers[c]);
                            }
                        }
                        mOffersAdapter.setOffers(listFavs);
                        try {
                            synchronized(this){
                                wait(2005);

                            }
                        }
                        catch(InterruptedException ex){
                        }

                    }
                };
                thread.start();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
*/
        return v;
    }

}

