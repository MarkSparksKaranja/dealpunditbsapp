package com.techmata.dealpundit.bs.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.techmata.dealpundit.bs.app.database.OffersDbHelper;
import com.techmata.dealpundit.bs.app.models.OfferItem;

/**
 * Created by Sparks on 17/08/2016.
 */
public class OfferActivity extends AppCompatActivity {

    ImageView offerImage;
    TextView title, description, company;
    OfferItem offer;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer);
        int pos= getIntent().getExtras().getInt("offer",-1);
        if (pos != -1 ){
            offer = OffersFragment.listOffers.get(pos);
        }else {
            Integer offerID = getIntent().getExtras().getInt("offerID",-1);
            //Toast.makeText(OfferActivity.this,"Offer ID -> "+offerID,Toast.LENGTH_SHORT).show();
            if(offerID != -1){
                OffersDbHelper mDB = new OffersDbHelper(OfferActivity.this);
                offer = mDB.getOffer(offerID);
            }else {
               // moveTaskToBack(true);
                finish();
                return;
            }
        }

        offerImage = (ImageView) findViewById(R.id.offerImage);
        if (offer.getImgData() != null){
            //Toast.makeText(BsActivity.this,"Image There",Toast.LENGTH_SHORT).show();
            offerImage.setVisibility(View.VISIBLE);
            Glide.with(OfferActivity.this)
                    .load(offer.getImg())
                    .asBitmap()
                    .placeholder(R.drawable.image_holder)
                    .into(offerImage);
        } else {
            //Toast.makeText(BsActivity.this,"Image Null",Toast.LENGTH_SHORT).show();
            offerImage.setVisibility(View.GONE);
        }

        title = (TextView) findViewById(R.id.offerTitle);
        title.setText(offer.getOffer_name());

        description = (TextView) findViewById(R.id.offerDesc);
        description.setText(offer.getOffer_des());

        company = (TextView) findViewById(R.id.offerCompany);
        if(offer.getCompany() != null){
            company.setText(offer.getCompany().getBsName());
        }else {
            company.setText("Company");
        }

        ActionBar tlb = getSupportActionBar();
        tlb.setDisplayHomeAsUpEnabled(true);
        tlb.setHomeButtonEnabled(true);
        tlb.setTitle(offer.getOffer_name());

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_offer, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, offer.getOffer_name() + ":\n" +offer.getOffer_des()
                    + "\n"+ "Shared from Deal Pundit" );
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, "Share offer"));
            return true;
        }else if(id == android.R.id.home){
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
