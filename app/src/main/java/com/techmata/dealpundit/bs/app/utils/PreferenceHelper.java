package com.techmata.dealpundit.bs.app.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.widget.Toast;
import com.techmata.dealpundit.bs.app.auth.AccountActivity;

public class PreferenceHelper {

    private static SharedPreferences mSharedPreferences;

    public static boolean setAccessToken(String token, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("access_token", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("access_token", token);
        return editor.commit();
    }

    public static String getAccessToken(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("access_token", Context.MODE_PRIVATE);
        return mSharedPreferences.getString("access_token", null);
    }

    public static boolean setRefreshToken(String token, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("refresh_token", token);
        return editor.commit();
    }

    public static String getRefreshToken(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("settings", Context.MODE_PRIVATE);
        return mSharedPreferences.getString("refresh_token", null);
    }

    public static boolean setEmail(String email, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("email", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("email", email);
        return editor.commit();
    }
    public static String getEmail(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("email", Context.MODE_PRIVATE);
        return mSharedPreferences.getString("email", "");
    }
    public static boolean setType(int ID, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("type", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt("type", ID);
        return editor.commit();
    }

    public static int getType(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("type", 0);
        return mSharedPreferences.getInt("type", 0);
    }

    public static boolean setHasPage(int ID, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("page", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt("page", ID);
        return editor.commit();
    }

    public static int getHasPage(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("page", 0);
        return mSharedPreferences.getInt("page", 0);
    }

    public static void logOut(Context mContext, @Nullable String message) {
        if(message == null){
            message = "You have been logged out";
        }
        setAccessToken(null, mContext);
        setRefreshToken(null, mContext);
        setHasPage(0, mContext);
        setType(0, mContext);
        setHasInterests(0, mContext);
        Intent intent = new Intent(mContext, AccountActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        mContext.startActivity(intent);
        Toast.makeText(mContext,message,Toast.LENGTH_SHORT).show();
        //mContext.getApplicationContext().finish();
    }

    public static boolean setHasInterests(int ID, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("interests", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt("interests", ID);
        return editor.commit();
    }

    public static int getHasInterests(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("interests", 0);
        return mSharedPreferences.getInt("interests", 0);
    }
}
