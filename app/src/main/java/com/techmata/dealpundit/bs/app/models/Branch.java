package com.techmata.dealpundit.bs.app.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.widget.Toast;
import com.google.android.gms.maps.model.LatLng;
import com.techmata.dealpundit.bs.app.database.OffersContract;
import com.techmata.dealpundit.bs.app.database.OffersDbHelper;

/**
 * Created by Sparks on 26/03/2016.
 */
public class Branch {
    private long branch_id;
    private int bs_id;
    private String branchName, locName;
    private LatLng location;
    private Context mContext;

    public Branch(Context c){
        this.mContext = c;
    }

    public Branch(int bs_id, String branchName,String locName, LatLng location) {
        this.bs_id = bs_id;
        this.branchName = branchName;
        this.location = location;
        this.locName = locName;
    }

    public Branch(long branch_id, int bs_id, String branchName, String locName, LatLng location) {
        this.branch_id = branch_id;
        this.bs_id = bs_id;
        this.branchName = branchName;
        this.locName = locName;
        this.location = location;
    }

    //TODO allow it to update changes to branch
    public void saveBranch (){
        new SaveData().execute();
    }
    private class SaveData extends AsyncTask<String, Void, Void> {

        protected Void doInBackground(final String... val) {
            SQLiteDatabase db = new OffersDbHelper(mContext).getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(OffersContract.MyBranches.COLUMN_NAME_BRANCH_NAME,branchName);
            values.put(OffersContract.MyBranches.COLUMN_NAME_BUSINESS_ID,bs_id);
            values.put(OffersContract.MyBranches.COLUMN_NAME_LOC_NAME,locName);
            values.put(OffersContract.MyBranches.COLUMN_NAME_LAT,String.valueOf(location.latitude));
            values.put(OffersContract.MyBranches.COLUMN_NAME_LON,String.valueOf(location.longitude));

            db.insert(
                    OffersContract.MyBranches.TABLE_NAME,null,
                    values);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(mContext,"Data Saved",Toast.LENGTH_SHORT).show();

        }
    }

    public int getBs_id() {
        return bs_id;
    }

    public void setBs_id(int bs_id) {
        this.bs_id = bs_id;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getLocName() {
        return locName;
    }

    public void setLocName(String locName) {
        this.locName = locName;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public long getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(long branch_id) {
        this.branch_id = branch_id;
    }
    /*public String getDateTime(int type){
        String datetime = null;
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-DD HH:MM", Locale.getDefault());
        Calendar cal = Calendar.getInstance();
        if(type == 0){//start time
            cal.set(Calendar.HOUR_OF_DAY, fTime.get(Calendar.HOUR_OF_DAY));
            cal.set(Calendar.MINUTE, fTime.get(Calendar.MINUTE));
            cal.set(Calendar.DAY_OF_MONTH, fDate.get(Calendar.MONTH));
            cal.set(Calendar.MONTH, fDate.get(Calendar.MONTH));
            cal.set(Calendar.YEAR, fDate.get(Calendar.YEAR));
            datetime = sdf.format(cal.getTime());
        }else if(type == 1){//stop time
            cal.set(Calendar.HOUR_OF_DAY, tTime.get(Calendar.HOUR_OF_DAY));
            cal.set(Calendar.MINUTE, tTime.get(Calendar.MINUTE));
            cal.set(Calendar.DAY_OF_MONTH, tDate.get(Calendar.MONTH));
            cal.set(Calendar.MONTH, tDate.get(Calendar.MONTH));
            cal.set(Calendar.YEAR, tDate.get(Calendar.YEAR));
            datetime = sdf.format(cal);
        }
        Log.d("myDateTime",datetime);
        return datetime;
    }*/

}
