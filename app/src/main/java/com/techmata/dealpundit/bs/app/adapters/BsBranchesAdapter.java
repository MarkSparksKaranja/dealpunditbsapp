package com.techmata.dealpundit.bs.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.techmata.dealpundit.bs.app.R;
import com.techmata.dealpundit.bs.app.models.Branch;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Sparks on 26/03/2016.
 */

public class BsBranchesAdapter extends RecyclerView.Adapter<BsBranchesAdapter.ViewHolderBsBranch> {

    //contains the list of posts
    private ArrayList<Integer> mATLists = new ArrayList<Integer>();
    private ArrayList<Branch> myBranches = new ArrayList<Branch>();
    private LayoutInflater mInflater;
    Context mcontext;
    String timeStamp;
    ViewHolderBsBranch viewHolder;
    Date date;
    int mMode = 0;

    public BsBranchesAdapter(Context context,ArrayList<Branch> mBranches) {
        this.mcontext = context;
        myBranches = mBranches;
        mInflater = LayoutInflater.from(context);
        mATLists.add(0);
        myBranches.add(0, new Branch(mcontext));
    }

    public BsBranchesAdapter(Context context,ArrayList<Branch> mBranches,int mMode) {
        this.mcontext = context;
        myBranches = mBranches;
        mInflater = LayoutInflater.from(context);
        this.mMode = mMode;
        if (mMode == 0){
            mATLists.add(0);
            myBranches.add(0, new Branch(mcontext));
        }
    }
    public void addBranch(int a) {
        Branch bsBranch = new Branch(mcontext);
        //add everything
        myBranches.add(bsBranch);
        //update the adapter to reflect the new set of posts
        notifyDataSetChanged();
    }

    public void addBranch(int a,Branch b) {
        myBranches.add(b);
        //update the adapter to reflect the new set of posts
        notifyDataSetChanged();
    }
    public ArrayList<Branch> getMyBranches(){
       return myBranches;
    }

    @Override
    public ViewHolderBsBranch onCreateViewHolder(final ViewGroup parent, int viewType) {
        final View view = mInflater.inflate(R.layout.item_bs_branch, parent, false);
        final ViewHolderBsBranch vHolder = new ViewHolderBsBranch(view,mcontext);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolderBsBranch holder, final int position) {

        this.viewHolder = holder;
        /*SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try {
            date = formatter.parse(currentActivity.getTimestamp().substring(0, 24));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        timeStamp = (String) DateUtils.getRelativeTimeSpanString(date.getTime(),
                System.currentTimeMillis(),
                DateUtils.SECOND_IN_MILLIS);
                */
        //Toast.makeText(mcontext,"Bind view",Toast.LENGTH_SHORT).show();

        if(mMode == 1){
            holder.etBsName.setEnabled(false);
            holder.etBsName.setFocusable(false);
            holder.etBsLoc.setEnabled(false);
            holder.etBsLoc.setFocusable(false);

            holder.btnClose.setVisibility(View.GONE);
        }
        if(myBranches.get(position).getBranchName() != null){
            holder.etBsName.setText(myBranches.get(position).getBranchName());
        }
        if(myBranches.get(position).getLocName() != null){
            holder.etBsLoc.setText(myBranches.get(position).getLocName());
        }
    }

    @Override
    public int getItemCount() {
        return myBranches.size();
    }

    public boolean removeAt(int position) {
        if(myBranches.size() <= 1 ){
            Toast.makeText(mcontext,"Has to be at least one",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            myBranches.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, myBranches.size());
            return true;
        }
    }

    public void updateLoc(Integer pos, String name, LatLng loc){
        myBranches.get(pos).setLocName(name);
        myBranches.get(pos).setLocation(loc);
        notifyDataSetChanged();
    }

    class ViewHolderBsBranch extends RecyclerView.ViewHolder {

        EditText etBsName, etBsLoc;
        ImageButton btnClose;

        public ViewHolderBsBranch(final View itemView, final Context mcontext) {
            super(itemView);
            etBsName = (EditText) itemView.findViewById(R.id.etBrName);
            etBsName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    //Toast.makeText(mcontext,"Edited "+getAdapterPosition(),Toast.LENGTH_SHORT).show();
                    myBranches.get(getAdapterPosition()).setBranchName(etBsName.getText().toString());
                    return false;
                }
            });

            etBsName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    myBranches.get(getAdapterPosition()).setBranchName(etBsName.getText().toString());
                }
            });
            etBsLoc = (EditText) itemView.findViewById(R.id.etBrLoc);
            etBsLoc.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    //builder.setLatLngBounds(new LatLngBounds(MyApplication.SW, MyApplication.NE));
                    try {
                        ((Activity) mcontext).startActivityForResult(builder.build((Activity) mcontext),getAdapterPosition());
                    } catch (GooglePlayServicesRepairableException e) {
                        e.printStackTrace();
                    } catch (GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                    }
                }
            });

            btnClose = (ImageButton) itemView.findViewById(R.id.btnClose);
            btnClose.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setVisibility(View.GONE);
                    removeAt(getAdapterPosition());
                    v.setVisibility(View.VISIBLE);
                }
            });
        }
    }
}

