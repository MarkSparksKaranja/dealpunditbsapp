package com.techmata.dealpundit.bs.app.models;

import android.content.Context;

import java.util.Calendar;

/**
 * Created by Sparks on 26/03/2016.
 */
public class OfferTime {
    private Calendar fDate, fTime, tDate, tTime;
    private Context mContext;
    private long actID;
    private Integer timeID;

    public OfferTime(Context c){
        this.mContext = c;
    }
    public OfferTime(Calendar fDate, Calendar fTime, Calendar tDate, Calendar tTime){
        this.fDate = fDate;
        this.fTime = fTime;
        this.tDate = tDate;
        this.tTime = tTime;
    }
    public OfferTime(Integer timeID,Calendar fDate, Calendar fTime, Calendar tDate, Calendar tTime){
        this.timeID = timeID;
        this.fDate = fDate;
        this.fTime = fTime;
        this.tDate = tDate;
        this.tTime = tTime;
    }

    public Integer getTimeID() {
        return timeID;
    }

    public void setTimeID(Integer timeID) {
        this.timeID = timeID;
    }

    /*public String getDateTime(int type){
        String datetime = null;
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-DD HH:MM", Locale.getDefault());
        Calendar cal = Calendar.getInstance();
        if(type == 0){//start time
            cal.set(Calendar.HOUR_OF_DAY, fTime.get(Calendar.HOUR_OF_DAY));
            cal.set(Calendar.MINUTE, fTime.get(Calendar.MINUTE));
            cal.set(Calendar.DAY_OF_MONTH, fDate.get(Calendar.MONTH));
            cal.set(Calendar.MONTH, fDate.get(Calendar.MONTH));
            cal.set(Calendar.YEAR, fDate.get(Calendar.YEAR));
            datetime = sdf.format(cal.getTime());
        }else if(type == 1){//stop time
            cal.set(Calendar.HOUR_OF_DAY, tTime.get(Calendar.HOUR_OF_DAY));
            cal.set(Calendar.MINUTE, tTime.get(Calendar.MINUTE));
            cal.set(Calendar.DAY_OF_MONTH, tDate.get(Calendar.MONTH));
            cal.set(Calendar.MONTH, tDate.get(Calendar.MONTH));
            cal.set(Calendar.YEAR, tDate.get(Calendar.YEAR));
            datetime = sdf.format(cal);
        }
        Log.d("myDateTime",datetime);
        return datetime;
    }*/
    public void setfDate(Calendar fDate) {
        this.fDate = fDate;
    }

    public void setfTime(Calendar fTime) {
        this.fTime = fTime;
    }

    public void settDate(Calendar tDate) {
        this.tDate = tDate;
    }

    public void settTime(Calendar tTime) {
        this.tTime = tTime;
    }

    public Calendar getfDate() {
        return fDate;
    }

    public Calendar getfTime() {
        return fTime;
    }

    public Calendar gettDate() {
        return tDate;
    }

    public Calendar gettTime() {
        return tTime;
    }
}
