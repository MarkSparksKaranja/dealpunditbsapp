package com.techmata.dealpundit.bs.app.business;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.android.volley.*;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.techmata.dealpundit.bs.app.R;
import com.techmata.dealpundit.bs.app.adapters.BsBranchesAdapter;
import com.techmata.dealpundit.bs.app.database.OffersDbHelper;
import com.techmata.dealpundit.bs.app.models.Branch;
import com.techmata.dealpundit.bs.app.models.Business;
import com.techmata.dealpundit.bs.app.utils.FilePath;
import com.techmata.dealpundit.bs.app.utils.MyApplication;
import com.techmata.dealpundit.bs.app.utils.PreferenceHelper;
import com.theartofdev.edmodo.cropper.CropImage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sparks on 02/10/2016.
 */
public class BsActivity extends AppCompatActivity {
    private static final int SELECT_PIC = 2020;
    private static final int SELECT_FILE = 2030;

    EditText etBsName, etBsDes;
    Spinner spCat;
    Button btnRegister;
    RecyclerView rvBranches;
    ProgressBar pgBar;
    BsBranchesAdapter adapterBranches;
    OffersDbHelper mDB;
    TextView tvPrivacy;
    ImageView ivBs;

    String selectedPhotoImagePath = null;

    long bsID;

    int MODE;
    public static int ADD_MODE = 0;
    public static int VIEW_MODE = 1;
    public static int EDIT_MODE = 2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_bs);

        MODE = getIntent().getIntExtra("MODE",-1);

        ActionBar tlb = getSupportActionBar();
        tlb.setDisplayHomeAsUpEnabled(true);
        tlb.setHomeButtonEnabled(true);

        //COMMON ACTIONS
        etBsName = (EditText) findViewById(R.id.bsName);
        etBsDes = (EditText) findViewById(R.id.bsDesc);

        rvBranches = (RecyclerView) findViewById(R.id.rvBranches);
        LinearLayoutManager layoutManager = new LinearLayoutManager(BsActivity.this, LinearLayoutManager.VERTICAL, false);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvBranches.setLayoutManager(layoutManager);

        ivBs = (ImageView) findViewById(R.id.bsAvatar);

        //SHARED VIEW & EDIT
        if(MODE > ADD_MODE){
            TextView tvCat = (TextView) findViewById(R.id.tvCategory);
            tvCat.setVisibility(View.GONE);
            //get business ID
            bsID = getIntent().getLongExtra("bsID",-1);
            //Toast.makeText(BsActivity.this,"mybsID -> "+bsID,Toast.LENGTH_SHORT).show();
            if(bsID >= 0) {
                OffersDbHelper mDB = new OffersDbHelper(BsActivity.this);
                Business mBusiness = mDB.getMyBusiness(bsID, true);

                tlb.setTitle(mBusiness.getBsName());

                //NAME & DES
                etBsName.setText(mBusiness.getBsName());
                etBsDes.setText(mBusiness.getBsDes());

                //CATEGORIES
                spCat = (Spinner) findViewById(R.id.spCategory);
                spCat.setVisibility(View.GONE);

                //BRANCHES
                adapterBranches = new BsBranchesAdapter(BsActivity.this, mBusiness.getBranches(),MODE);
                rvBranches.setAdapter(adapterBranches);

                //PRIVACY
                tvPrivacy = (TextView) findViewById(R.id.privacy);
                tvPrivacy.setVisibility(View.GONE);

                //IMAGE
                if (mBusiness.getImgData() != null){
                    //Toast.makeText(BsActivity.this,"Image There",Toast.LENGTH_SHORT).show();
                    ivBs.setVisibility(View.VISIBLE);
                    Glide.with(BsActivity.this)
                            .load(mBusiness.getImg())
                            .asBitmap()
                            .placeholder(R.drawable.factory_30)
                            .into(ivBs);
                } else {
                   //Toast.makeText(BsActivity.this,"Image Null",Toast.LENGTH_SHORT).show();
                    ivBs.setVisibility(View.GONE);
                }
            }
        }
        //SHARED ADD & EDIT
        if (MODE == ADD_MODE || MODE == EDIT_MODE){
            Button btnAddTime = (Button) findViewById(R.id.btnAddBranch);
            btnAddTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapterBranches.addBranch(adapterBranches.getItemCount());
                }
            });

            //CATEGORIES
            HashMap<String, Integer> categories = new OffersDbHelper(BsActivity.this).getCategories();
            String[] spItems = new String[categories.size()];
            int c = 0;
            for (String key : categories.keySet()) {
                spItems[c] = key;
                c++;
            }
            ArrayAdapter<String> spAdapter = new ArrayAdapter<String>
                    (BsActivity.this, android.R.layout.simple_spinner_dropdown_item, spItems);
            spCat = (Spinner) findViewById(R.id.spCategory);
            spCat.setAdapter(spAdapter);

            ivBs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(
                            Intent.createChooser(intent,"Choose Image"),
                            SELECT_PIC);
                    /*Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(galleryIntent, SELECT_FILE);*/
                }
            });
        }

        if (MODE == ADD_MODE) {//ADD MODE
            //BRANCHES
            ArrayList<Branch> mBranches = new ArrayList<Branch>();
            adapterBranches = new BsBranchesAdapter(BsActivity.this, mBranches);
            rvBranches.setAdapter(adapterBranches);


            //REGISTER BUTTON
            btnRegister = (Button) findViewById(R.id.btnCreate);
            btnRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (etBsName.getText().toString().isEmpty()) {
                        etBsName.setError("Branch name cannot be empty");
                    } else if (etBsDes.getText().toString().isEmpty()) {
                        etBsDes.setError("Enter a etDescription");
                    } else {
                        String imgData = null;
                        if (selectedPhotoImagePath != null){
                            Bitmap selectedImage =  BitmapFactory.decodeFile(selectedPhotoImagePath);
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            selectedImage.compress(Bitmap.CompressFormat.JPEG, 70, stream);
                            byte[] byteArray = stream.toByteArray();
                            imgData = Base64.encodeToString(byteArray, 0);
                            String filename = selectedPhotoImagePath.substring(selectedPhotoImagePath.lastIndexOf("/")+1);
                        }

                        for (Branch mBranch : adapterBranches.getMyBranches()) {
                            if (mBranch.getBranchName() == null) {
                                Toast.makeText(BsActivity.this, "Enter all branch details", Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                        pgBar = (ProgressBar) findViewById(R.id.pbBs);
                        pgBar.setVisibility(View.VISIBLE);
                        btnRegister.setVisibility(View.GONE);

                        createBusiness(new Business(etBsName.getText().toString(), etBsDes.getText().toString(),
                                adapterBranches.getMyBranches(),imgData),false);
                    }
                }
            });
        }else if(MODE == VIEW_MODE){//VIEW MODE
            if(bsID >= 0){
                setTitle("View Business");
                etBsName.setEnabled(false);
                etBsName.setFocusable(false);
                etBsDes.setEnabled(false);
                etBsDes.setFocusable(false);

                Button btnAddTime = (Button) findViewById(R.id.btnAddBranch);
                btnAddTime.setVisibility(View.GONE);


                //REGISTER BUTTON
                btnRegister = (Button) findViewById(R.id.btnCreate);
                btnRegister.setVisibility(View.GONE);
            }else{
                Log.i("myBsId","business ID not set");
            }
        }else if(MODE == EDIT_MODE){//EDIT MODE
            //get business ID
            bsID = getIntent().getLongExtra("bsID",-1);
            if(bsID >= 0){
                setTitle("Edit Business");

                //SAVE BUTTON
                btnRegister = (Button) findViewById(R.id.btnCreate);
                btnRegister.setText("Save Changes");
                btnRegister.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (etBsName.getText().toString().isEmpty()) {
                            etBsName.setError("Branch name cannot be empty");
                        } else if (etBsDes.getText().toString().isEmpty()) {
                            etBsDes.setError("Enter a etDescription");
                        } else {
                            String imgData = null;
                            if (selectedPhotoImagePath != null){
                                Bitmap selectedImage =  BitmapFactory.decodeFile(selectedPhotoImagePath);
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                selectedImage.compress(Bitmap.CompressFormat.JPEG, 70, stream);
                                byte[] byteArray = stream.toByteArray();
                                imgData = Base64.encodeToString(byteArray, 0);
                                String filename = selectedPhotoImagePath.substring(selectedPhotoImagePath.lastIndexOf("/")+1);
                            }

                            for (Branch mBranch : adapterBranches.getMyBranches()) {
                                if (mBranch.getBranchName() == null) {
                                    Toast.makeText(BsActivity.this, "Enter all branch details", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                            }
                            pgBar = (ProgressBar) findViewById(R.id.pbBs);
                            pgBar.setVisibility(View.VISIBLE);
                            btnRegister.setVisibility(View.GONE);

                            //final long id  = bsID;
                            createBusiness(new Business(bsID,etBsName.getText().toString(), etBsDes.getText().toString(),
                                    adapterBranches.getMyBranches(),imgData),true);
                        }
                    }
                });
            }else {
                Log.i("myBsId","business ID not set");
            }

        }
    }

    private void createBusiness(final Business mBusiness, final boolean update){
        JSONObject jsonParam = new JSONObject();

        try {
            if(update){
                jsonParam.put("bs_id",mBusiness.getId());
            }
            jsonParam.put("name",mBusiness.getBsName());
            jsonParam.put("description",mBusiness.getBsDes());
            JSONArray branches = new JSONArray();
            for (Branch mBranch: mBusiness.getBranches() ) {
                JSONObject branch = new JSONObject();
                if(update){
                    branch.put("branch_id",mBranch.getBranch_id());
                }
                branch.put("branch_name",mBranch.getBranchName());
                branch.put("location_meta",(mBranch.getLocName()));
                branch.put("lat",(mBranch.getLocation().latitude));
                branch.put("lon",(mBranch.getLocation().longitude));
                branches.put(branch);
            }
            //TODO ensure add branches is working
            Log.i("branches", branches.toString());
            jsonParam.put("branches", branches);
            //for edit, check if image has actually been changed
            jsonParam.put("image", mBusiness.getImgData());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("myparam",jsonParam.toString());
        // return;
        int method;
        if(update){
            method = Request.Method.PUT;
        }else {
            method = Request.Method.POST;
        }
        JsonObjectRequest jsonRequest = new JsonObjectRequest(method,
                MyApplication.URL+"/businesses",
                jsonParam,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if(response != null){
                            try {
                                Log.i("myresp",response.toString(2));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (response.has("bs_id")){
                                //Toast.makeText(getContext(),"Ride Requested",Toast.LENGTH_LONG).show();
                                mDB = new OffersDbHelper(BsActivity.this);
                                try {
                                    mBusiness.setId(response.getInt("bs_id"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    JSONArray branches = response.getJSONArray("branches");
                                    if(update){
                                        mDB.deleteBusiness(mBusiness.getId(),false);
                                    }
                                    mDB.addBusiness(mBusiness,branches,update);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.i("myBranches","error parsing branches");
                                }

                                pgBar.setVisibility(View.GONE);
                                btnRegister.setVisibility(View.VISIBLE);

                                //TODO launch bsViewActivity (BsActivity in view mode)
                                Intent intent = new Intent(BsActivity.this, BsActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("MODE", 1);
                                intent.putExtra("bsID",mBusiness.getId() );
                                startActivity(intent);
                            } else {
                                Log.i("myerr","invalid response, no offer_id");
                                String err = "Error";
                                if (response.has("error")){
                                    try {
                                        err = response.getString("error");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                Toast.makeText(BsActivity.this,err,Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(BsActivity.this,"Error, please try again.",Toast.LENGTH_LONG).show();
                        }
                        pgBar.setVisibility(View.GONE);
                        btnRegister.setVisibility(View.VISIBLE);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("myerr",error.toString());
                try {
                    String response = new String(error.networkResponse.data,"UTF-8");
                    Log.i("myerrRes",response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Toast.makeText(BsActivity.this,"Error",Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                pgBar.setVisibility(View.GONE);
                btnRegister.setVisibility(View.VISIBLE);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();
                params.put("Content-Type", "application/json");
                params.put("Accept", "application/json");
                params.put("Authorisation","Bearer "+ PreferenceHelper.getAccessToken(BsActivity.this));
                Log.i("myHeaders", params.toString());
                return params;
            }
        };
        MyApplication myApp = new MyApplication(BsActivity.this);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,3,DefaultRetryPolicy.DEFAULT_MAX_RETRIES));
        MyApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK) {
                Uri croppedURI = result.getUri();
                //productImage.setImageURI(croppedURI);
                selectedPhotoImagePath = FilePath.getPath(BsActivity.this, croppedURI);

                Log.i("myImagePath", selectedPhotoImagePath);
                if (selectedPhotoImagePath != null) {
                    Glide.with(BsActivity.this)
                            .load(new File(selectedPhotoImagePath))
                            .into(ivBs);
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(BsActivity.this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
            return;
        }
        if (requestCode == SELECT_PIC) {
            if (resultCode == Activity.RESULT_OK) {
                //selectedPhotoImagePath = FileUtils.getPath(getApplicationContext(), data.getData());
                // croppedURI =  Uri.fromFile(new File(getContext().getCacheDir(), "dpAdmin"));
                // Crop.of(data.getData(), croppedURI).asSquare().start(getActivity())
                CropImage.activity(data.getData())
                        .setAspectRatio(1,1)
                        .start(BsActivity.this);

            }else {
                Toast.makeText(BsActivity.this,"Error selecting image",Toast.LENGTH_SHORT).show();
            }
            return;
        }
        if(resultCode == Activity.RESULT_OK){
            Place selectedPlace = PlacePicker.getPlace(BsActivity.this,data);
            adapterBranches.updateLoc(requestCode,handlePlace(selectedPlace),selectedPlace.getLatLng());
            /*adapterBranches.getTimes().get(requestCode).setLocation(selectedPlace.getLatLng());
            adapterBranches.updateLoc();*/
            //adapterBranches.addBranch((requestCode+1),new Branch(1,"My Branch",selectedPlace.getLatLng()));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if(MODE == VIEW_MODE){
            getMenuInflater().inflate(R.menu.menu_business,menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home){
           // moveTaskToBack(false);
            finish();
            return true;
        }
        if(MODE == VIEW_MODE){
            if(item.getItemId() == R.id.action_edit){
                Intent intent = new Intent(BsActivity.this, BsActivity.class);
                intent.putExtra("MODE", EDIT_MODE);
                intent.putExtra("bsID",bsID);
                startActivity(intent);
                finish();
            }else if(item.getItemId() == R.id.action_catalog){
                Intent intent = new Intent(BsActivity.this, CatalogActivity.class);
                intent.putExtra("MODE", EDIT_MODE);
                intent.putExtra("bsID",bsID);
                startActivity(intent);
                finish();
            }
        }
        return true;
    }

    private String handlePlace(Place selectedPlace){
        String text;
        //Log.i("myPlace", selectedPlace.toString());
        if (selectedPlace.getName() != null){
            text = selectedPlace.getName().toString();
            if (selectedPlace.getAddress() != null){
                text += ", "+selectedPlace.getAddress().toString();
            }
        }else {
            if (selectedPlace.getAddress() != null){
                text = selectedPlace.getAddress().toString();
            }else {
                text = selectedPlace.getLatLng().toString();
            }
        }
        return text;
    }
}
