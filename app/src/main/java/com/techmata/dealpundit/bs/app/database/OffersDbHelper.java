package com.techmata.dealpundit.bs.app.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;
import com.android.volley.*;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.model.LatLng;
import com.techmata.dealpundit.bs.app.database.OffersContract.Categories;
import com.techmata.dealpundit.bs.app.database.OffersContract.Offers;
import com.techmata.dealpundit.bs.app.database.OffersContract.UserDetails;
import com.techmata.dealpundit.bs.app.models.*;
import com.techmata.dealpundit.bs.app.utils.MyApplication;
import com.techmata.dealpundit.bs.app.utils.PreferenceHelper;
import com.techmata.dealpundit.bs.app.utils.ServiceCallback;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by SparkSoft on 18/03/2015.
 */
public class OffersDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "techmata.dealpundit.bs.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private final Context mContext;

    SimpleDateFormat sdfDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.getDefault());
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd",Locale.getDefault());
    SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss",Locale.getDefault());

    Cursor res;
    SQLiteDatabase db;

    ContentValues values = new ContentValues();

    public OffersDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + UserDetails.TABLE_NAME+ " (" +
                UserDetails.COLUMN_NAME_DETAIL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                UserDetails.COLUMN_NAME_DETAIL_NAME+ TEXT_TYPE + COMMA_SEP +
                UserDetails.COLUMN_NAME_DETAIL_VALUE + TEXT_TYPE +
                " )");
        db.execSQL( "CREATE TABLE IF NOT EXISTS " + Categories.TABLE_NAME+ " (" +
                Categories.COLUMN_NAME_CATEGORY_ID + " INTEGER PRIMARY KEY ," +
                Categories.COLUMN_NAME_CATEGORY_NAME + TEXT_TYPE  + COMMA_SEP +
                Categories.COLUMN_NAME_CATEGORY_DES + TEXT_TYPE +
                " )");

        db.execSQL( "CREATE TABLE IF NOT EXISTS " + Offers.TABLE_NAME+ " (" +
                Offers.COLUMN_NAME_OFFER_ID+ " INTEGER PRIMARY KEY ," +
                Offers.COLUMN_NAME_OFFER_NAME + TEXT_TYPE  + COMMA_SEP +
                Offers.COLUMN_NAME_OFFER_DES + TEXT_TYPE  + COMMA_SEP +
                Offers.COLUMN_NAME_CAT_ID + " INTEGER ,"+
                Offers.COLUMN_NAME_IMG_DATA + " BLOB ,"+
                Offers.COLUMN_NAME_BS_ID + " INTEGER ,"+
                Offers.COLUMN_NAME_BS_NAME + TEXT_TYPE  + COMMA_SEP +
                Offers.COLUMN_NAME_FAV + " INTEGER ,"+
                Offers.COLUMN_NAME_FEATURED + " INTEGER "+
                " )");
        db.execSQL( "CREATE TABLE IF NOT EXISTS " + OffersContract.BusinessCategories.TABLE_NAME+ " (" +
                OffersContract.BusinessCategories.COLUMN_NAME_CATEGORY_ID + " INTEGER PRIMARY KEY ," +
                OffersContract.BusinessCategories.COLUMN_NAME_BS_ID + " INTEGER ,"+
                OffersContract.BusinessCategories.COLUMN_NAME_CATEGORY_NAME + TEXT_TYPE  + COMMA_SEP +
                OffersContract.BusinessCategories.COLUMN_NAME_CATEGORY_DES + TEXT_TYPE +
                " )");
        db.execSQL( "CREATE TABLE IF NOT EXISTS " + OffersContract.Products.TABLE_NAME+ " (" +
                OffersContract.Products.COLUMN_NAME_PRODUCT_ID + " INTEGER PRIMARY KEY ," +
                OffersContract.Products.COLUMN_NAME_PRODUCT_NAME + TEXT_TYPE  + COMMA_SEP +
                OffersContract.Products.COLUMN_NAME_PRODUCT_DES + TEXT_TYPE  + COMMA_SEP +
                OffersContract.Products.COLUMN_NAME_CAT_ID + " INTEGER ,"+
                OffersContract.Products.COLUMN_NAME_IMG_DATA + " BLOB ,"+
                OffersContract.Products.COLUMN_NAME_BS_ID + " INTEGER ,"+
                OffersContract.Products.COLUMN_NAME_BS_NAME + TEXT_TYPE  + COMMA_SEP +
                OffersContract.Products.COLUMN_NAME_PRICE + " INTEGER "+
                " )");
        db.execSQL( "CREATE TABLE IF NOT EXISTS " + OffersContract.MyBusinesses.TABLE_NAME+ " (" +
                OffersContract.MyBusinesses.COLUMN_NAME_BUSINESS_ID+ " INTEGER PRIMARY KEY," +
                OffersContract.MyBusinesses.COLUMN_NAME_CATEGORY_ID+ " INTEGER ," +
                OffersContract.MyBusinesses.COLUMN_NAME_BUSINESS_NAME + TEXT_TYPE  + COMMA_SEP +
                OffersContract.MyBusinesses.COLUMN_NAME_IMG + " BLOB "+ COMMA_SEP +
                OffersContract.MyBusinesses.COLUMN_NAME_REG_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP , "+
                OffersContract.MyBusinesses.COLUMN_NAME_DES + TEXT_TYPE +
                " )");
        db.execSQL( "CREATE TABLE IF NOT EXISTS " + OffersContract.MyBranches.TABLE_NAME+ " (" +
                OffersContract.MyBranches.COLUMN_NAME_BRANCH_ID+ " INTEGER PRIMARY KEY," +
                OffersContract.MyBranches.COLUMN_NAME_BRANCH_NAME + TEXT_TYPE  + COMMA_SEP +
                OffersContract.MyBranches.COLUMN_NAME_BUSINESS_ID+ " INTEGER ," +
                OffersContract.MyBranches.COLUMN_NAME_LOC_NAME + TEXT_TYPE  + COMMA_SEP +
                OffersContract.MyBranches.COLUMN_NAME_LAT + TEXT_TYPE  + COMMA_SEP +
                OffersContract.MyBranches.COLUMN_NAME_LON + TEXT_TYPE +
                " )");
        db.execSQL( "CREATE TABLE IF NOT EXISTS " + OffersContract.OffersTimes.TABLE_NAME+ " (" +
                OffersContract.OffersTimes.COLUMN_NAME_TIME_ID+ " INTEGER PRIMARY KEY AUTOINCREMENT," +
                OffersContract.OffersTimes.COLUMN_NAME_OFFER_ID + " INTEGER " + COMMA_SEP +
                OffersContract.OffersTimes.COLUMN_NAME_START_DATE +" DATETIME "+ COMMA_SEP +
                OffersContract.OffersTimes.COLUMN_NAME_START_TIME +" DATETIME "+ COMMA_SEP +
                OffersContract.OffersTimes.COLUMN_NAME_STOP_DATE +" DATETIME "+ COMMA_SEP +
                OffersContract.OffersTimes.COLUMN_NAME_STOP_TIME +" DATETIME "+
                " )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public int getOfferCount(){
        String countQuery = "SELECT  * FROM " + Offers.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }
    public int getOfferCount(Integer catID){
        String countQuery = "SELECT * FROM "+ OffersContract.Offers.TABLE_NAME +
                " WHERE "+ Offers.COLUMN_NAME_CAT_ID +" = "+catID;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

    public void addOffer(Integer offerID,String name, String des, Business myBusiness, String imgData, boolean fav, ArrayList<OfferTime> times, Integer catID) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        //TODO add id column
        values.put(Offers.COLUMN_NAME_OFFER_NAME,name);
        values.put(Offers.COLUMN_NAME_OFFER_NAME,name);
        values.put(Offers.COLUMN_NAME_OFFER_DES,des);
        if (myBusiness != null ){
            values.put(Offers.COLUMN_NAME_BS_ID ,myBusiness.getId());
            values.put(Offers.COLUMN_NAME_BS_NAME ,myBusiness.getBsName());
        }else {
            values.put(Offers.COLUMN_NAME_BS_ID ,0);
            values.put(Offers.COLUMN_NAME_BS_NAME ,(String) null);
        }
        values.put(Offers.COLUMN_NAME_IMG_DATA,imgData);
        values.put(Offers.COLUMN_NAME_FAV, fav);
        values.put(Offers.COLUMN_NAME_CAT_ID, catID);

        db.insert(
                OffersContract.Offers.TABLE_NAME,null,
                values);
        db.close();
    }

    public void addOffer(OfferItem offer, JSONArray offer_times){
        ContentValues values = new ContentValues();
        //TODO add id column
        values.put(Offers.COLUMN_NAME_OFFER_ID,offer.getOffer_id());
        values.put(Offers.COLUMN_NAME_OFFER_NAME,offer.getOffer_name());
        values.put(Offers.COLUMN_NAME_OFFER_DES,offer.getOffer_des());
        if (offer.getCompany() != null ){
            values.put(Offers.COLUMN_NAME_BS_ID ,offer.getCompany().getId());
            values.put(Offers.COLUMN_NAME_BS_NAME ,offer.getCompany().getBsName());
        }else {
            values.put(Offers.COLUMN_NAME_BS_ID ,0);
            values.put(Offers.COLUMN_NAME_BS_NAME ,(String) null);
        }
        values.put(Offers.COLUMN_NAME_IMG_DATA,offer.getImgData());
        values.put(Offers.COLUMN_NAME_FAV, offer.getFav());
        values.put(Offers.COLUMN_NAME_CAT_ID, offer.getCat_id());

        for (int t  = 0; t < offer_times.length();t++){
            //TODO add offer times
            JSONObject offer_time = null;
            try {
                offer_time = offer_times.getJSONObject(t);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            saveOfferTime(offer.getOffer_id(),offer_time);
        }


        SQLiteDatabase db = this.getWritableDatabase();

        db.insert(
                OffersContract.Offers.TABLE_NAME,null,
                values);
        db.close();
    }
    private void saveOffer(JSONObject offer) {
        Integer offerID = null;

        String mNull = null;
        values = new ContentValues();
        try {
            offerID = offer.getInt("offer_id");
            values.put(Offers.COLUMN_NAME_OFFER_ID, offerID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(offerID!=null){

            //Log.i("gotTrip",trip.getString("end"))

            valuesAdd(values,Offers.COLUMN_NAME_OFFER_NAME, offer, "offer_name", 1);
            valuesAdd(values,Offers.COLUMN_NAME_OFFER_DES, offer, "offer_des", 1);
            valuesAdd(values,Offers.COLUMN_NAME_BS_ID, offer, "bs_id", 0);
            valuesAdd(values,Offers.COLUMN_NAME_CAT_ID, offer, "cat_id", 0);
            valuesAdd(values,Offers.COLUMN_NAME_IMG_DATA, offer, "img_data", 1);
            //valuesAdd(Offers.COLUMN_NAME_FAV, offer, "fav", 4);
            values.put(Offers.COLUMN_NAME_FAV,0);
            valuesAdd(values,Offers.COLUMN_NAME_FEATURED, offer, "featured", 0);

            JSONArray offer_times = new JSONArray();
            try {
                offer_times = offer.getJSONArray("offer_times");
                for (int t  = 0; t < offer_times.length();t++){
                    //TODO add offer times
                    JSONObject offer_time = offer_times.getJSONObject(t);
                    saveOfferTime(offerID,offer_time);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            SQLiteDatabase mDB = this.getWritableDatabase();

            mDB.insert(
                    OffersContract.Offers.TABLE_NAME,
                    null,
                    values);
            // mDB.close();
        }
        values = null;

    }
    public void saveOfferTime(Integer offerID,JSONObject offer_time){

        ContentValues mValues = new ContentValues();

        valuesAdd(mValues,OffersContract.OffersTimes.COLUMN_NAME_TIME_ID, offer_time, "time_id", 0);
        mValues.put(OffersContract.OffersTimes.COLUMN_NAME_OFFER_ID,offerID);
        //valuesAdd(mValues,OffersContract.OffersTimes.COLUMN_NAME_OFFER_ID, offer_time, "offer_id", 0);
        valuesAdd(mValues,OffersContract.OffersTimes.COLUMN_NAME_START_DATE, offer_time, "start_date", 1);
        valuesAdd(mValues,OffersContract.OffersTimes.COLUMN_NAME_START_TIME, offer_time, "start_time", 1);
        valuesAdd(mValues,OffersContract.OffersTimes.COLUMN_NAME_STOP_DATE, offer_time, "stop_date", 1);
        valuesAdd(mValues,OffersContract.OffersTimes.COLUMN_NAME_STOP_TIME, offer_time, "stop_time", 1);

        SQLiteDatabase mDB = this.getWritableDatabase();

        mDB.insert(
                OffersContract.OffersTimes.TABLE_NAME,
                null,
                mValues);
        mDB.close();
    }


    public OfferItem getOffer(long offerID){
        OfferItem offer = new OfferItem();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+ OffersContract.Offers.TABLE_NAME
                +" WHERE "+ Offers.COLUMN_NAME_OFFER_ID +" = "+offerID,null);
        if(cursor.moveToFirst()){
            final Integer id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(Offers.COLUMN_NAME_OFFER_ID)));
            final Integer cat_id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(Offers.COLUMN_NAME_CAT_ID)));
            final String name = cursor.getString(cursor.getColumnIndex(Offers.COLUMN_NAME_OFFER_NAME));
            final String des = cursor.getString(cursor.getColumnIndex(Offers.COLUMN_NAME_OFFER_DES));
            final String img_data = cursor.getString(cursor.getColumnIndex(Offers.COLUMN_NAME_IMG_DATA));
            Integer bs_id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(Offers.COLUMN_NAME_BS_ID)));
            //final Integer fav = Integer.parseInt(cursor.getString(cursor.getColumnIndex(Offers.COLUMN_NAME_FAV)));
            final Integer fav = 0;
            final Boolean favourite;
            if ( fav  == 0 ){
                favourite = Boolean.FALSE;
            }else {
                favourite = Boolean.TRUE;
            }
            ArrayList<OfferTime> mOfferTimes = getOfferTimes(id);

            //TODO have a Business table storing all the businesses?
            if ( bs_id  == 0 ){
                bs_id = null;
                offer = new OfferItem(id,name,des,null,img_data,favourite,mOfferTimes,cat_id);

            }else{
                offer = new OfferItem(id,name,des,getMyBusiness(bs_id,false),img_data,favourite,mOfferTimes,cat_id);
            }

        }
        cursor.close();

        return offer;
    }
    public boolean offerExists(Integer offerID){
        String countQuery = "SELECT "+Offers.COLUMN_NAME_OFFER_ID+" FROM "+ OffersContract.Offers.TABLE_NAME
                + " WHERE "+ Offers.COLUMN_NAME_OFFER_ID +" = "+offerID;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        db.close();
        if(cnt == 1){
            return true;
        }
        return false;
    }
    public void fetchOffers (final ServiceCallback callback){
        JsonArrayRequest jsonRequest = new JsonArrayRequest(Request.Method.GET,
                MyApplication.URL+"/offers/myoffers",
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        if(response != null){
                            Log.i("myOffersRes", response.toString());
                            //Drop table
                            //db.execSQL("DROP TABLE IF EXISTS "+ Offers.TABLE_NAME);
                            //TODO save fav in server
                            //HACK to get fav to resave it
                            for(int c = 0;c < response.length(); c++){
                                try {
                                    JSONObject offer = response.getJSONObject(c);
                                    Integer offerID = offer.getInt("offer_id");
                                    //check if offer exists in db
                                    offer.put("fav",0);
                                    if(offerExists(offerID)){
                                        // deleteOffer(offerID,false);
                                        offer.put("fav",0);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            new OffersDbHelper(mContext).getWritableDatabase().delete(Offers.TABLE_NAME,null,null);
                            new OffersDbHelper(mContext).getWritableDatabase().delete(OffersContract.OffersTimes.TABLE_NAME,null,null);
                            for(int c = 0;c < response.length(); c++){
                                try {
                                    JSONObject offer = response.getJSONObject(c);
                                    Integer offerID = offer.getInt("offer_id");
                                    saveOffer(offer);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            callback.onSuccess();
                        }else{
                            Toast.makeText(mContext,"Error updating Offers.",Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyApplication.getInstance().onErrorResponse(mContext,error,"Error updating Offers.");
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();
                params.put("Content-Type", "application/json");
                params.put("Accept", "application/json");
                params.put("Authorisation","Bearer "+PreferenceHelper.getAccessToken(mContext));
                Log.i("myHeaders", params.toString());
                return params;
            }
        };
        MyApplication myApp = new MyApplication(mContext);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy());
        MyApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    public OfferItem[] getOffers(@Nullable Integer catID){
        OfferItem[] offers;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor;
        if (catID != null){
            offers = new OfferItem[getOfferCount(catID)];
            Log.i("myCATID", "myCatdID "+catID);
            cursor = db.rawQuery("SELECT * FROM "+ OffersContract.Offers.TABLE_NAME +
                    " WHERE "+ Offers.COLUMN_NAME_CAT_ID +" = "+catID
                    +" ORDER BY "+Offers.COLUMN_NAME_OFFER_ID + " DESC ",null);
        }else{
            offers = new OfferItem[getOfferCount()];
            cursor = db.rawQuery("SELECT * FROM "+ OffersContract.Offers.TABLE_NAME+
                    " ORDER BY "+Offers.COLUMN_NAME_OFFER_ID + " DESC ",null);
        }
        if(cursor.moveToFirst()){
            while (!cursor.isAfterLast()) {
                final Integer id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(Offers.COLUMN_NAME_OFFER_ID)));
                final Integer cat_id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(Offers.COLUMN_NAME_CAT_ID)));
                final String name = cursor.getString(cursor.getColumnIndex(Offers.COLUMN_NAME_OFFER_NAME));
                final String des = cursor.getString(cursor.getColumnIndex(Offers.COLUMN_NAME_OFFER_DES));
                final String img_data = cursor.getString(cursor.getColumnIndex(Offers.COLUMN_NAME_IMG_DATA));
                Integer bs_id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(Offers.COLUMN_NAME_BS_ID)));
                final Integer fav = Integer.parseInt(cursor.getString(cursor.getColumnIndex(Offers.COLUMN_NAME_FAV)));
                final Boolean favourite;
                if ( fav  == 0  || fav == null){
                    favourite = Boolean.FALSE;
                }else {
                    favourite = Boolean.TRUE;
                }

                ArrayList<OfferTime> mOfferTimes = null;//getOfferTimes(id);

                //TODO have a Business table storing all the businesses?
                if ( bs_id  == 0 ){
                    bs_id = null;
                    offers[cursor.getPosition()] = new OfferItem(id,name,des,null,img_data,favourite,mOfferTimes,cat_id);
                }else{
                    offers[cursor.getPosition()] = new OfferItem(id,name,des,getMyBusiness(bs_id,false),img_data,favourite,mOfferTimes,cat_id);
                }

                cursor.moveToNext();
            }
        }else{
/*            mContext.getParerunOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(mContext,"No Data",Toast.LENGTH_SHORT).show();
                }
            });*/
        }
        cursor.close();

        return offers;
    }

    public ArrayList<OfferTime> getOfferTimes(Integer offerID){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor timesCursor = db.rawQuery("SELECT * FROM "+ OffersContract.OffersTimes.TABLE_NAME +
                " WHERE "+ OffersContract.OffersTimes.COLUMN_NAME_OFFER_ID +" = "+offerID ,null);
        //Log.i("myTimes","Offer => "+offerID+", "+timesCursor.getCount()+" times");
        ArrayList<OfferTime> offerTimes = new ArrayList<OfferTime>();

        if(timesCursor.getCount() == 0){
            offerTimes.add(getSampleOfferTime());
            return offerTimes;
        }

        if(timesCursor.moveToFirst()) {
            while (!timesCursor.isAfterLast()) {
                //Log.i("myTimesCursor","Position => "+timesCursor.getPosition());
                final Integer time_id = timesCursor.getInt(timesCursor.getColumnIndex(
                        OffersContract.OffersTimes.COLUMN_NAME_TIME_ID));
                final Integer offer_id = timesCursor.getInt(timesCursor.getColumnIndex(
                        OffersContract.OffersTimes.COLUMN_NAME_OFFER_ID));
                final String start_date = timesCursor.getString(timesCursor.getColumnIndex(
                        OffersContract.OffersTimes.COLUMN_NAME_START_DATE));
                final String start_time = timesCursor.getString(timesCursor.getColumnIndex(
                        OffersContract.OffersTimes.COLUMN_NAME_START_TIME));
                final String stop_date = timesCursor.getString(timesCursor.getColumnIndex(
                        OffersContract.OffersTimes.COLUMN_NAME_STOP_DATE));
                final String stop_time = timesCursor.getString(timesCursor.getColumnIndex(
                        OffersContract.OffersTimes.COLUMN_NAME_STOP_TIME));

                Calendar calfDate = Calendar.getInstance();
                Date fDate = null;
                try {
                    fDate = sdfDate.parse(start_date);
                    //calStart.setTime(sdfDate.parse(start_date));
                    calfDate.set(sdfDate.getCalendar().get(Calendar.YEAR),
                            sdfDate.getCalendar().get(Calendar.MONTH),
                            sdfDate.getCalendar().get(Calendar.DAY_OF_MONTH));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Calendar calfTime = Calendar.getInstance();
                Date fTime = null;
                try {
                    fTime = sdfTime.parse(start_time);
                    calfTime.set(Calendar.HOUR_OF_DAY, sdfTime.getCalendar().get(Calendar.HOUR_OF_DAY));
                    calfTime.set(Calendar.MINUTE, sdfTime.getCalendar().get(Calendar.MINUTE));
                    calfTime.set(Calendar.SECOND, sdfTime.getCalendar().get(Calendar.SECOND));
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                Calendar caltDate = Calendar.getInstance();
                Date tDate = null;
                try {
                    tDate = sdfDate.parse(stop_date);
                    caltDate.set(sdfDate.getCalendar().get(Calendar.YEAR),
                            sdfDate.getCalendar().get(Calendar.MONTH),
                            sdfDate.getCalendar().get(Calendar.DAY_OF_MONTH));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Calendar caltTime = Calendar.getInstance();
                Date tTime = null;
                try {
                    tTime = sdfTime.parse(stop_time);
                    caltTime.set(Calendar.HOUR_OF_DAY, sdfTime.getCalendar().get(Calendar.HOUR_OF_DAY));
                    caltTime.set(Calendar.MINUTE, sdfTime.getCalendar().get(Calendar.MINUTE));
                    caltTime.set(Calendar.SECOND, sdfTime.getCalendar().get(Calendar.SECOND));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                offerTimes.add(new OfferTime(time_id, calfDate, calfTime, caltDate, caltTime));
                timesCursor.moveToNext();
            }
        }else {
            offerTimes.add(getSampleOfferTime());
        }
        timesCursor.close();

        return offerTimes;
    }
    public OfferTime getSampleOfferTime(){
        Calendar calfDate = Calendar.getInstance();
        Date fDate = null;
        try {
            fDate = sdfDate.parse("0000-00-00");
            //calStart.setTime(sdfDate.parse(start_date));
            calfDate.set(sdfDate.getCalendar().get(Calendar.YEAR),
                    sdfDate.getCalendar().get(Calendar.MONTH),
                    sdfDate.getCalendar().get(Calendar.DAY_OF_MONTH));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calfTime = Calendar.getInstance();
        Date fTime = null;
        try {
            fTime = sdfTime.parse("00:00:00");
            calfTime.set(Calendar.HOUR_OF_DAY, sdfTime.getCalendar().get(Calendar.HOUR_OF_DAY));
            calfTime.set(Calendar.MINUTE, sdfTime.getCalendar().get(Calendar.MINUTE));
            calfTime.set(Calendar.SECOND, sdfTime.getCalendar().get(Calendar.SECOND));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Calendar caltDate = Calendar.getInstance();
        Date tDate = null;
        try {
            tDate = sdfDate.parse("0000-00-00");
            caltDate.set(sdfDate.getCalendar().get(Calendar.YEAR),
                    sdfDate.getCalendar().get(Calendar.MONTH),
                    sdfDate.getCalendar().get(Calendar.DAY_OF_MONTH));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar caltTime = Calendar.getInstance();
        Date tTime = null;
        try {
            tTime = sdfTime.parse("00:00:00");
            caltTime.set(Calendar.HOUR_OF_DAY, sdfTime.getCalendar().get(Calendar.HOUR_OF_DAY));
            caltTime.set(Calendar.MINUTE, sdfTime.getCalendar().get(Calendar.MINUTE));
            caltTime.set(Calendar.SECOND, sdfTime.getCalendar().get(Calendar.SECOND));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new OfferTime(0, calfDate, calfTime, caltDate, caltTime);
    }

    public void updateOffer(OfferItem offer){
        //TODO: UPDATE in DB using ID
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Offers.COLUMN_NAME_OFFER_NAME,offer.getOffer_name());
        values.put(Offers.COLUMN_NAME_OFFER_DES,offer.getOffer_des());
        if (offer.getCompany() != null ){
            values.put(Offers.COLUMN_NAME_BS_ID ,offer.getCompany().getId());
            values.put(Offers.COLUMN_NAME_BS_NAME ,offer.getCompany().getBsName());
        }else {
            values.put(Offers.COLUMN_NAME_BS_ID ,0);
            values.put(Offers.COLUMN_NAME_BS_NAME ,(String) null);
        }
        values.put(Offers.COLUMN_NAME_IMG_DATA,offer.getImgData());
        values.put(Offers.COLUMN_NAME_FAV, offer.getFav());

        db.update(
                OffersContract.Offers.TABLE_NAME,
                values,Offers.COLUMN_NAME_OFFER_ID+"="+offer.getOffer_id(),null);
        db.close();
    }

    public void addCategory(String catName,String catDes){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Categories.COLUMN_NAME_CATEGORY_NAME,catName);
        values.put(Categories.COLUMN_NAME_CATEGORY_DES,catDes);

        db.insert(
                OffersContract.Categories.TABLE_NAME,null,
                values);
        db.close();
    }
    public HashMap<String,Integer> getCategories(){
        HashMap<String,Integer> cats = new HashMap<String, Integer>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+ OffersContract.Categories.TABLE_NAME ,null);
        if(cursor.moveToFirst()){
            while (!cursor.isAfterLast()) {
                final Integer id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(Categories.COLUMN_NAME_CATEGORY_ID)));
                final String name = cursor.getString(cursor.getColumnIndex(Categories.COLUMN_NAME_CATEGORY_NAME));
                final String des = cursor.getString(cursor.getColumnIndex(Categories.COLUMN_NAME_CATEGORY_DES));

                cats.put(name,id);
                cursor.moveToNext();
            }
        }
        db.close();
        return cats;
    }
    public int getCatCount(){
        /*db = this.getReadableDatabase();
        return (int) DatabaseUtils.queryNumEntries(db, Offers.TABLE_NAME, "type= ? ", new String[]{Integer.toString(type)});*/
        String countQuery = "SELECT  * FROM " + Categories.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        db.close();
        return cnt;
    }
    public void addBranch(long branchID, String brName, long bsID, String locName, LatLng loc){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(OffersContract.MyBranches.COLUMN_NAME_BRANCH_ID,branchID);
        values.put(OffersContract.MyBranches.COLUMN_NAME_BRANCH_NAME,brName);
        values.put(OffersContract.MyBranches.COLUMN_NAME_BUSINESS_ID,bsID);
        values.put(OffersContract.MyBranches.COLUMN_NAME_LOC_NAME,locName);
        values.put(OffersContract.MyBranches.COLUMN_NAME_LAT,String.valueOf(loc.latitude));
        values.put(OffersContract.MyBranches.COLUMN_NAME_LON,String.valueOf(loc.longitude));

        db.insert(
                OffersContract.MyBranches.TABLE_NAME,null,
                values);
        db.close();
    }
    public boolean deleteBranch(long branchID, boolean api){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+ OffersContract.MyBranches.TABLE_NAME +
                " WHERE " + OffersContract.MyBranches.COLUMN_NAME_BRANCH_ID+" = "+branchID);
        db.close();
        if(api){
            //TODO fix delete bs from api
            final SQLiteDatabase mDb = this.getWritableDatabase();
            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.DELETE,
                    MyApplication.URL+"/trips/"+String.valueOf(branchID),
                    null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if(response != null){
                                Log.i("myRes", response.toString());

                            }else{
                                Toast.makeText(mContext,"Error updating projects.",Toast.LENGTH_LONG).show();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("myerr",error.toString());
                    try {
                        String response = new String(error.networkResponse.data,"UTF-8");
                        Log.i("myerrRes",response);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(mContext,"Error",Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String,String>();
                    params.put("Content-Type", "application/json");
                    params.put("Accept", "application/json");
                    params.put("Authorisation","Bearer "+ PreferenceHelper.getAccessToken(mContext));
                    Log.i("myHeaders", params.toString());
                    return params;
                }
            };
            MyApplication myApp = new MyApplication(mContext);
            jsonRequest.setRetryPolicy(new DefaultRetryPolicy());
            MyApplication.getInstance().addToRequestQueue(jsonRequest);

        }
        return true;
    }
    public ArrayList<Branch> getMyBranches (long id){
        ArrayList<Branch> mBranches = new ArrayList<Branch>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+ OffersContract.MyBranches.TABLE_NAME
                +" WHERE "+ OffersContract.MyBranches.COLUMN_NAME_BUSINESS_ID +" = "+id,null);
        if(cursor.moveToFirst()){
            while (!cursor.isAfterLast()) {
                final Integer rID = cursor.getInt(cursor.getColumnIndex(OffersContract.MyBranches.COLUMN_NAME_BRANCH_ID));
                final String rName = cursor.getString(cursor.getColumnIndex(OffersContract.MyBranches.COLUMN_NAME_BRANCH_NAME));
                final String rLName = cursor.getString(cursor.getColumnIndex(OffersContract.MyBranches.COLUMN_NAME_LOC_NAME));
                final Double rLat = cursor.getDouble(cursor.getColumnIndex(OffersContract.MyBranches.COLUMN_NAME_LAT));
                final Double rLon = cursor.getDouble(cursor.getColumnIndex(OffersContract.MyBranches.COLUMN_NAME_LON));
                mBranches.add(new Branch(rID,rName,rLName,new LatLng(rLon,rLat)));
                cursor.moveToNext();
            }
        }
        db.close();
        return mBranches;
    }

    public int getMyBsCount(){
        String countQuery = "SELECT  * FROM " + OffersContract.MyBusinesses.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        db.close();
        return cnt;
    }

    public void addBusiness(Integer id,String bsName,String des,ArrayList<Branch> mBranches,String imgData,boolean update){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        //TODO add id column
        values.put(OffersContract.MyBusinesses.COLUMN_NAME_BUSINESS_ID,id);
        values.put(OffersContract.MyBusinesses.COLUMN_NAME_BUSINESS_NAME,bsName);
        values.put(OffersContract.MyBusinesses.COLUMN_NAME_CATEGORY_ID,1);
        values.put(OffersContract.MyBusinesses.COLUMN_NAME_DES,des);
        values.put(OffersContract.MyBusinesses.COLUMN_NAME_IMG,imgData);

        db.insert(
                OffersContract.MyBusinesses.TABLE_NAME,null,
                values);
        for (Branch mBranch :mBranches) {
            if(update) {
                deleteBranch(mBranch.getBranch_id(),false);
            }
            addBranch(mBranch.getBranch_id(),
                    mBranch.getBranchName(),
                    id,
                    mBranch.getLocName(),
                    mBranch.getLocation());
        }

        db.close();
    }
    public void addBusiness(Business mBusiness,JSONArray branches,boolean update){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(OffersContract.MyBusinesses.COLUMN_NAME_BUSINESS_ID,mBusiness.getId());
        values.put(OffersContract.MyBusinesses.COLUMN_NAME_BUSINESS_NAME,mBusiness.getBsName());
        values.put(OffersContract.MyBusinesses.COLUMN_NAME_DES,mBusiness.getBsDes());
        values.put(OffersContract.MyBusinesses.COLUMN_NAME_CATEGORY_ID,1);
        values.put(OffersContract.MyBusinesses.COLUMN_NAME_IMG,mBusiness.getImgData());

        db.insert(
                OffersContract.MyBusinesses.TABLE_NAME,null,
                values);
        for (int c = 0; c < branches.length(); c++) {
            JSONObject mBranch = null;
            try {
                mBranch = branches.getJSONObject(c);
                if(update) {
                    deleteBranch(mBranch.getInt("branch_id"),false);
                }
                addBranch(mBranch.getInt("branch_id"),
                        mBranch.getString("branch_name"),
                        mBranch.getInt("bs_id"),
                        mBranch.getString("location_meta"),
                        new LatLng(mBranch.getDouble("lat"), mBranch.getDouble("lon")));
            } catch (JSONException e) {
                e.printStackTrace();
            }

    }

        db.close();
    }
    public int getMyBsCount(long bsID){
        String countQuery = "SELECT * FROM "+ OffersContract.MyBusinesses.TABLE_NAME
                + " WHERE "+ OffersContract.MyBusinesses.COLUMN_NAME_BUSINESS_ID +" = "+bsID;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        db.close();
        return cnt;
    }
    public Business getMyBusiness(long bsID,boolean branches){
        Business mBusiness = null;
        if(getMyBsCount(bsID) > 0){
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery("SELECT * FROM "+ OffersContract.MyBusinesses.TABLE_NAME
                    + " WHERE "+ OffersContract.MyBusinesses.COLUMN_NAME_BUSINESS_ID +" = "+bsID,null);
            if(cursor.moveToFirst()){
                final Integer id = cursor.getInt(cursor.getColumnIndex(OffersContract.MyBusinesses.COLUMN_NAME_BUSINESS_ID));
                final String name = cursor.getString(cursor.getColumnIndex(OffersContract.MyBusinesses.COLUMN_NAME_BUSINESS_NAME));
                final String des = cursor.getString(cursor.getColumnIndex(OffersContract.MyBusinesses.COLUMN_NAME_DES));
                final String imgData = cursor.getString(cursor.getColumnIndex(OffersContract.MyBusinesses.COLUMN_NAME_IMG));
                if(branches){
                    mBusiness = new Business(id,name,des,getMyBranches(id),imgData);
                }else {
                    mBusiness = new Business(id,name,des);
                }
            }
            cursor.close();
            db.close();
        }
        return mBusiness;
    }

    public ArrayList<Business> getMyBusinesses(boolean branches){
        ArrayList<Business> mBusinesses = new ArrayList<Business>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+ OffersContract.MyBusinesses.TABLE_NAME ,null);
        if(cursor.moveToFirst()){
            while (!cursor.isAfterLast()) {
                final Integer id = cursor.getInt(cursor.getColumnIndex(OffersContract.MyBusinesses.COLUMN_NAME_BUSINESS_ID));
                final String name = cursor.getString(cursor.getColumnIndex(OffersContract.MyBusinesses.COLUMN_NAME_BUSINESS_NAME));
                final String des = cursor.getString(cursor.getColumnIndex(OffersContract.MyBusinesses.COLUMN_NAME_DES));
                final String imgData = cursor.getString(cursor.getColumnIndex(OffersContract.MyBusinesses.COLUMN_NAME_IMG));
                if(branches){
                    mBusinesses.add(new Business(id,name,des,getMyBranches(id),imgData));
                }else{
                    mBusinesses.add(new Business(id,name,des));
                }
                cursor.moveToNext();
            }
        }
        //cursor.close();
        db.close();
        return mBusinesses;
    }
    public boolean deleteBusiness(long bsID, boolean api){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+ OffersContract.MyBusinesses.TABLE_NAME +
                " WHERE " + OffersContract.MyBusinesses.COLUMN_NAME_BUSINESS_ID+" = "+bsID);
        db.close();
        if(api){
            //TODO fix delete bs from api
            final SQLiteDatabase mDb = this.getWritableDatabase();
            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.DELETE,
                    MyApplication.URL+"/trips/"+String.valueOf(bsID),
                    null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if(response != null){
                                Log.i("myRes", response.toString());

                            }else{
                                Toast.makeText(mContext,"Error updating projects.",Toast.LENGTH_LONG).show();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("myerr",error.toString());
                    try {
                        String response = new String(error.networkResponse.data,"UTF-8");
                        Log.i("myerrRes",response);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(mContext,"Error",Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String,String>();
                    params.put("Content-Type", "application/json");
                    params.put("Accept", "application/json");
                    params.put("Authorisation","Bearer "+ PreferenceHelper.getAccessToken(mContext));
                    Log.i("myHeaders", params.toString());
                    return params;
                }
            };
            MyApplication myApp = new MyApplication(mContext);
            jsonRequest.setRetryPolicy(new DefaultRetryPolicy());
            MyApplication.getInstance().addToRequestQueue(jsonRequest);

        }
        return true;
    }
    public void fetchMyBusinesses(){
        final SQLiteDatabase mDb = this.getWritableDatabase();
        JsonArrayRequest jsonRequest = new JsonArrayRequest(Request.Method.GET,
                MyApplication.URL+"/businesses/me",
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        if(response != null){
                            Log.i("myRes", response.toString());
                            //Drop table
                            new OffersDbHelper(mContext).getWritableDatabase().delete(OffersContract.MyBusinesses.TABLE_NAME,null,null);
                            new OffersDbHelper(mContext).getWritableDatabase().delete(OffersContract.MyBranches.TABLE_NAME,null,null);
                            for(int c = 0;c < response.length(); c++){
                                try {
                                    JSONObject business = response.getJSONObject(c);
                                    saveBusiness(business);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }else{
                            Toast.makeText(mContext,"Error updating businesses.",Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyApplication.getInstance().onErrorResponse(mContext,error,"Error updating businesses.");
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();
                params.put("Content-Type", "application/json");
                params.put("Accept", "application/json");
                params.put("Authorisation","Bearer "+ PreferenceHelper.getAccessToken(mContext));
                Log.i("myHeaders", params.toString());
                return params;
            }
        };
        MyApplication myApp = new MyApplication(mContext);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy());
        MyApplication.getInstance().addToRequestQueue(jsonRequest);
    }
    public void fetchCatalog (final long bsID, final ServiceCallback callback){
        JsonArrayRequest jsonRequest = new JsonArrayRequest(Request.Method.GET,
                MyApplication.URL+"/products/bs/"+String.valueOf(bsID),
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        if(response != null){
                            Log.i("myProductsRes", response.toString());
                            //Drop table
                            if(response.length() > 0){
                                new OffersDbHelper(mContext).getWritableDatabase()
                                        .delete(OffersContract.Products.TABLE_NAME,"bs_id=?",new String[]{String.valueOf(bsID)});
                                for(int c = 0;c < response.length(); c++){
                                    try {
                                        JSONObject prod = response.getJSONObject(c);
                                        Product mProduct = new Product(prod.getInt("product_id"),
                                            prod.getString("product_name"),
                                            prod.getString("product_des"),
                                            prod.getString("bs_id"),
                                            prod.getString("img_data"),
                                            prod.getInt("price"),
                                            prod.getInt("cat_id"));
                                        mProduct.setBs_id(bsID);
                                        addProduct(mProduct);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }else{
                            Toast.makeText(mContext,"Error updating catalog.",Toast.LENGTH_LONG).show();
                        }
                        callback.onSuccess();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyApplication.getInstance().onErrorResponse(mContext,error,"Error updating catalog.");
                callback.onSuccess();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();
                params.put("Content-Type", "application/json");
                params.put("Accept", "application/json");
                params.put("Authorisation","Bearer "+PreferenceHelper.getAccessToken(mContext));
                Log.i("myHeaders", params.toString());
                return params;
            }
        };
        MyApplication myApp = new MyApplication(mContext);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy());
        MyApplication.getInstance().addToRequestQueue(jsonRequest);
    }
    private void saveBusiness(JSONObject business) {
        Integer bsID = null;

        String mNull = null;
        values = new ContentValues();
        try {
            bsID = business.getInt("bs_id");
            values.put(OffersContract.MyBusinesses.COLUMN_NAME_BUSINESS_ID, bsID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(bsID!=null){
            //Log.i("gotTrip",trip.getString("end"))
            valuesAdd(values,OffersContract.MyBusinesses.COLUMN_NAME_BUSINESS_NAME, business, "bs_name", 1);
            valuesAdd(values,OffersContract.MyBusinesses.COLUMN_NAME_DES, business, "bs_desc", 1);
            valuesAdd(values,OffersContract.MyBusinesses.COLUMN_NAME_CATEGORY_ID, business, "category", 0);
            valuesAdd(values,OffersContract.MyBusinesses.COLUMN_NAME_IMG, business, "img_data", 1);

            JSONArray branches = new JSONArray();
            try {
                branches = business.getJSONArray("branches");
                for (int t  = 0; t < branches.length();t++){
                    //TODO save branches
                    JSONObject branch = branches.getJSONObject(t);
                    final long branch_id = branch.getInt("branch_id");
                    final String branch_name = branch.getString("branch_name");
                    final String loc_name = branch.getString("location_meta");
                    final Double lat = branch.getDouble("lat");
                    final Double lon = branch.getDouble("lon");
                    final LatLng location = new LatLng(lat,lon);

                    addBranch(branch_id,branch_name,bsID,loc_name, location);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            SQLiteDatabase mDB = this.getWritableDatabase();

            mDB.insert(
                    OffersContract.MyBusinesses.TABLE_NAME,
                    null,
                    values);
            // mDB.close();
        }
        values = null;

    }
    public ArrayList<Product> getMyBusinessCatalog(long bsID){
        ArrayList<Product> mCatalog = null;
        mCatalog = new ArrayList<Product>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+ OffersContract.Products.TABLE_NAME
                + " WHERE "+ OffersContract.MyBusinesses.COLUMN_NAME_BUSINESS_ID +" = "+bsID,null);
        if(cursor.moveToFirst()){
            while (!cursor.isAfterLast()){
                final Integer id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(OffersContract.Products.COLUMN_NAME_PRODUCT_ID)));
                final String name = cursor.getString(cursor.getColumnIndex(OffersContract.Products.COLUMN_NAME_PRODUCT_NAME));
                final String des = cursor.getString(cursor.getColumnIndex(OffersContract.Products.COLUMN_NAME_PRODUCT_DES));
                final Integer cat_id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(OffersContract.Products.COLUMN_NAME_CAT_ID)));
                final String img_data = cursor.getString(cursor.getColumnIndex(OffersContract.Products.COLUMN_NAME_IMG_DATA));
                final String bs_name = cursor.getString(cursor.getColumnIndex(OffersContract.Products.COLUMN_NAME_BS_NAME));
                Integer bs_id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(OffersContract.Products.COLUMN_NAME_BS_ID)));
                final Integer price = Integer.parseInt(cursor.getString(cursor.getColumnIndex(OffersContract.Products.COLUMN_NAME_PRICE)));
                Log.i("myProd", name);
                //Log.i("myProdPrice", price);
                mCatalog.add(new Product(id,name,des,bs_name,img_data,price,cat_id));
                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();
        return mCatalog;
    }

    public void addBsCategory(Integer bsID,Integer catID,String catName,String catDes){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(OffersContract.BusinessCategories.COLUMN_NAME_CATEGORY_ID,catID);
        values.put(OffersContract.BusinessCategories.COLUMN_NAME_BS_ID,bsID);
        values.put(OffersContract.BusinessCategories.COLUMN_NAME_CATEGORY_NAME,catName);
        values.put(OffersContract.BusinessCategories.COLUMN_NAME_CATEGORY_DES,catDes);

        db.insert(
                OffersContract.BusinessCategories.TABLE_NAME,null,
                values);
        db.close();
    }
    public HashMap<String,Integer> getBsCategories(Long bsID){
        HashMap<String,Integer> cats = new HashMap<String, Integer>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+ OffersContract.BusinessCategories.TABLE_NAME
                + " WHERE "+ OffersContract.BusinessCategories.COLUMN_NAME_BS_ID + " = "+bsID,null);
        if(cursor.moveToFirst()){
            while (!cursor.isAfterLast()) {
                final Integer id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(Categories.COLUMN_NAME_CATEGORY_ID)));
                final String name = cursor.getString(cursor.getColumnIndex(Categories.COLUMN_NAME_CATEGORY_NAME));
                final String des = cursor.getString(cursor.getColumnIndex(Categories.COLUMN_NAME_CATEGORY_DES));
                cats.put(name,id);
                cursor.moveToNext();
            }
        }
        db.close();
        return cats;
    }

    public void addProduct(Product product){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(OffersContract.Products.COLUMN_NAME_PRODUCT_ID,product.getProduct_id());
        values.put(OffersContract.Products.COLUMN_NAME_PRODUCT_NAME,product.getProduct_name());
        values.put(OffersContract.Products.COLUMN_NAME_PRODUCT_DES,product.getProduct_des());
        values.put(OffersContract.Products.COLUMN_NAME_PRICE,product.getPrice());
        values.put(OffersContract.Products.COLUMN_NAME_BS_ID,product.getBs_id());
        values.put(OffersContract.Products.COLUMN_NAME_CAT_ID,product.getCat_id());
        values.put(OffersContract.Products.COLUMN_NAME_IMG_DATA,product.getImgData());

        db.insert(
                OffersContract.Products.TABLE_NAME,null,
                values);
        db.close();
    }

    public Product getProduct(long productID){
        Product product = new Product();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+ OffersContract.Products.TABLE_NAME
                +" WHERE "+ OffersContract.Products.COLUMN_NAME_PRODUCT_ID +" = " + productID,null);
        if(cursor.moveToFirst()){
            final Integer product_id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(OffersContract.Products.COLUMN_NAME_PRODUCT_ID)));
            final Integer cat_id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(OffersContract.Products.COLUMN_NAME_CAT_ID)));
            final Integer bs_id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(OffersContract.Products.COLUMN_NAME_BS_ID)));
            final String name = cursor.getString(cursor.getColumnIndex(OffersContract.Products.COLUMN_NAME_PRODUCT_NAME));
            final String des = cursor.getString(cursor.getColumnIndex(OffersContract.Products.COLUMN_NAME_PRODUCT_DES));
            final Integer price = Integer.parseInt(cursor.getString(cursor.getColumnIndex(OffersContract.Products.COLUMN_NAME_PRICE)));
            final String img_data = cursor.getString(cursor.getColumnIndex(OffersContract.Products.COLUMN_NAME_IMG_DATA));

            String bs_name = getMyBusiness(bs_id,false).getBsName();

            product = new Product(product_id,name,des,bs_name,img_data,price,cat_id);

        }
        cursor.close();

        return product;
    }

    private void valuesAdd(ContentValues values,String dbName, JSONObject data,String dataName, Integer DATA_TYPE){
        if(!data.isNull(dataName)) {
            if(DATA_TYPE == 0){
                //Integer
                try {
                    values.put(dbName,data.getInt(dataName));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else if(DATA_TYPE == 1){
                //String
                try {
                    values.put(dbName,data.getString(dataName));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }else if(DATA_TYPE == 3){
                //JSON Array
                try {
                    values.put(dbName, String.valueOf(data.getJSONArray(dataName)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else if(DATA_TYPE == 4){
                //Boolean
                try {
                    values.put(dbName, data.getBoolean(dataName));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }else {
            String n = null;
            values.put(dbName,n);
        }
    }

}
