package com.techmata.dealpundit.bs.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.techmata.dealpundit.bs.app.R;
import com.techmata.dealpundit.bs.app.business.ProductActivity;
import com.techmata.dealpundit.bs.app.models.Product;

import java.util.ArrayList;

/**
 * Created by Sparks on 15/08/2016.
 */
public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolderProducts> {

    private final Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Product> mListProducts = new ArrayList<Product>();
    ViewHolderProducts viewHolder;


    public ProductsAdapter(Context context, ArrayList<Product> listPost) {
        this.mContext = context;
        this.mListProducts = listPost;
        mInflater = LayoutInflater.from(context);
    }

    public void setProducts(ArrayList<Product> listOffers) {
        this.mListProducts = listOffers;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolderProducts onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_product, parent, false);
        ViewHolderProducts viewHolder = new ViewHolderProducts(view,mContext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolderProducts holder, int position) {
        viewHolder = holder;

        Product currentProduct = mListProducts.get(position);

        holder.prodName.setText(currentProduct.getProduct_name());
        holder.prodPrice.setText("KES "+currentProduct.getPrice().toString());
        holder.prodDes.setText(currentProduct.getProduct_des());
        if (currentProduct.getImgData() != null){
            holder.prodImage.setVisibility(View.VISIBLE);
            Glide.with(mContext)
                    .load(currentProduct.getImg())
                    .asBitmap()
                    .placeholder(R.drawable.image_holder)
                    .into(holder.prodImage);
            /*Glide.with(mContext)
                    .load(currentProduct.getImg())
                    .asBitmap()
                    .centerCrop()
                    .placeholder(R.drawable.image_holder)
                    .error(R.drawable.image_holder)
                    .into(new BitmapImageViewTarget(holder.prodImage) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            holder.prodImage.setImageDrawable(circularBitmapDrawable);
                        }
                    });*/
        } else {
            //Toast.makeText(BsActivity.this,"Image Null",Toast.LENGTH_SHORT).show();
            holder.prodImage.setVisibility(View.GONE);
        }

       /**/
    }
    public boolean removeAt(int position) {
        mListProducts.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mListProducts.size());
        return true;
    }


    @Override
    public int getItemCount() {
        return mListProducts.size();
    }
    class ViewHolderProducts extends RecyclerView.ViewHolder {

        ImageView prodImage;
        TextView prodName, prodPrice, prodDes;

        public ViewHolderProducts(final View itemView, final Context mContext) {
            super(itemView);
            prodName = (TextView) itemView.findViewById(R.id.prodName);
            prodPrice = (TextView) itemView.findViewById(R.id.prodPrice);
            prodDes = (TextView) itemView.findViewById(R.id.prodDescription);
            prodImage = (ImageView) itemView.findViewById(R.id.productImage);


            View.OnClickListener openProd = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(mContext,"image "+String.valueOf(getAdapterPosition()),Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(mContext, ProductActivity.class);
                    intent.putExtra("product", getAdapterPosition());
                    mContext.startActivity(intent);
                }
            };
            prodImage.setOnClickListener(openProd);
            prodName.setOnClickListener(openProd);
            prodDes.setOnClickListener(openProd);
        }
    }
}
