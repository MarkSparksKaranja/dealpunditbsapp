package com.techmata.dealpundit.bs.app.utils;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;

public class MyApplication extends Application {

    public static String CLIENT_SECRET  = "7d2689ade5885f5f59caad220123097d726e9407a3b1d490381a75e5b98ccd74";
    public static String CLIENT_ID  = "f2e2ee094f51a74a7031b1e7f2558d286c647316406e251ac3cc14ad13427509";
    public static String URL  = "http://54.186.218.134/dealpundit/api";
    public static String URL0  = "http://192.168.43.42/dealpundit/api";
    public static String API_KEY = "1111";
    public static final String TAG = MyApplication.class.getSimpleName();
    private static RequestQueue mRequestQueue;
    private static MyApplication mInstance;
    private static Context context;


    public MyApplication(Context contx){
        context = contx;
        mInstance = this;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(this.context);
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }
    public static Integer pxTOdp(Integer px, Context context){
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int dp  = (int) Math.round(px/ (metrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }
    public static Float dpTOpx(Integer dp, Context context){
        Resources r = context.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        return px;
    }
    public void onErrorResponse(Context mContext,VolleyError error,String errorMessage){
        Log.i("myerr",error.toString());

        try {
            String response = new String(error.networkResponse.data,"UTF-8");
            Log.i("myerrRes",response);
            if(response.contains("invalid token")){
                PreferenceHelper.logOut(mContext,"Login has expired");
                return;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        Toast.makeText(mContext,errorMessage,Toast.LENGTH_SHORT).show();
        error.printStackTrace();

    }
}
