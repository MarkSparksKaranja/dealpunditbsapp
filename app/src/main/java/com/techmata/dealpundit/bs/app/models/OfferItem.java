package com.techmata.dealpundit.bs.app.models;

import android.util.Base64;

import java.util.ArrayList;

/**
 * Created by Sparks on 15/08/2016.
 */
public class OfferItem {
    int offer_id = 0, cat_id = 0;
    String offer_name = null, offer_des = null, imgData = null;
    Business company;
    Boolean fav = false;
    ArrayList<OfferTime> offerTimes = new ArrayList<OfferTime>();

    public OfferItem(String offer_name, String offer_des, Business company, String imgData, Boolean fav, ArrayList<OfferTime> offerTimes, int cat_id) {
        this.offer_name = offer_name;
        this.offer_des = offer_des;
        this.company = company;
        this.imgData = imgData;
        this.fav = fav;
        this.offerTimes = offerTimes;
        this.cat_id = cat_id;
    }
    public OfferItem(int offer_id, String offer_name, String offer_des, Business company, String imgData, Boolean fav,ArrayList<OfferTime> offerTimes,int cat_id) {
        this.offer_name = offer_name;
        this.offer_des = offer_des;
        this.company = company;
        this.imgData = imgData;
        this.offer_id = offer_id;
        this.cat_id = cat_id;
        this.fav = fav;

    }

    public OfferItem() {
    }



    public String getOffer_name() {
        return offer_name;
    }

    public void setOffer_name(String offer_name) {
        this.offer_name = offer_name;
    }

    public String getOffer_des() {
        return offer_des;
    }

    public void setOffer_des(String offer_des) {
        this.offer_des = offer_des;
    }

    public Business getCompany() {
        return company;
    }

    public void setCompany(Business company) {
        this.company = company;
    }

    public String getImgData() {
        return imgData;
    }
    public byte[] getImg(){
        try{
        return Base64.decode(imgData, Base64.DEFAULT);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
            return  null;
        }
    }

    public void setImgData(String imgData) {
        this.imgData = imgData;
    }

    public int getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(int offer_id) {
        this.offer_id = offer_id;
    }

    public Boolean getFav() {
        return fav;
    }

    public void setFav(Boolean fav) {
        this.fav = fav;
    }

    public ArrayList<OfferTime> getOfferTimes() {
        return offerTimes;
    }

    public void setOfferTimes(ArrayList<OfferTime> offerTimes) {
        this.offerTimes = offerTimes;
    }

    public int getCat_id() {
        return cat_id;
    }

    public void setCat_id(int cat_id) {
        this.cat_id = cat_id;
    }
}
