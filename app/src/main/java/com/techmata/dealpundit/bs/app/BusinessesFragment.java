package com.techmata.dealpundit.bs.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.techmata.dealpundit.bs.app.business.BsActivity;
import com.techmata.dealpundit.bs.app.database.OffersDbHelper;
import com.techmata.dealpundit.bs.app.models.Business;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Sparks on 15/08/2016.
 */
public class BusinessesFragment extends Fragment {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    public FloatingActionButton fabCreateBs;
    private ListView bsList;
    ArrayAdapter<String> bsAdapter;
    String[] itemsBs;
    HashMap<String,Long> spBusinesses;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View v =  inflater.inflate(R.layout.fragment_businesses,null);

        fabCreateBs = (FloatingActionButton) v.findViewById(R.id.fabAddBs);
        fabCreateBs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), BsActivity.class);
                intent.putExtra("MODE", BsActivity.ADD_MODE);
                startActivity(intent);
            }
        });

        TextView mNone = (TextView) v.findViewById(R.id.tvNone);
        //MY BUSINESS
        ArrayList<Business> mBusinesses = new OffersDbHelper(getContext()).getMyBusinesses(false);
        if (mBusinesses.size() == 0){
            mNone.setVisibility(View.VISIBLE);
            return v;
        }
        spBusinesses = new HashMap<String, Long>();
        itemsBs = new String[mBusinesses.size()];
        Integer c = 0;
        for (Business mBS: mBusinesses) {
            spBusinesses.put(mBS.getBsName(),mBS.getId());
            itemsBs[c] = mBS.getBsName();
           // Log.i("myBs",itemsBs[c]);
            c++;
        }
        bsAdapter = new ArrayAdapter<String>
                (getContext(), android.R.layout.simple_spinner_dropdown_item, itemsBs);
        bsList = (ListView) v.findViewById(R.id.listBs);
        bsList.setAdapter(bsAdapter);


        bsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i("myBs","myBiz " +  itemsBs[position] + "->"+ spBusinesses.get(itemsBs[position]));
                Intent intent = new Intent(getContext(), BsActivity.class);
                intent.putExtra("MODE", BsActivity.VIEW_MODE);
                intent.putExtra("bsID", spBusinesses.get(itemsBs[position]));
                startActivity(intent);
            }
        });


        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeBs);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Thread thread=  new Thread(){
                    @Override
                    public void run(){
                        /*ArrayList<Business> mBusinesses = new OffersDbHelper(getContext()).getMyBusinesses(false);
                        if (mBusinesses.size() != 0){
                            final HashMap<String,Long> spBusinesses = new HashMap<String, Long>();
                            itemsBs = new String[mBusinesses.size()];
                            Integer c = 0;
                            for (Business mBS: mBusinesses) {
                                spBusinesses.put(mBS.getBsName(),mBS.getId());
                                itemsBs[c] = mBS.getBsName();
                                c++;
                            }
                            bsAdapter.notifyDataSetChanged();
                        }
                        mSwipeRefreshLayout.setRefreshing(false);*/
                        try {
                            synchronized(this){
                                wait(3000);
                            }
                        }
                        catch(InterruptedException ex){
                        }

                    }
                };
                thread.start();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });


        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        ArrayList<Business> mBusinesses = new OffersDbHelper(getContext()).getMyBusinesses(false);
        Log.i("myBusinessesSize",String.valueOf(mBusinesses.size()));
        if (mBusinesses.size() != 0){
            spBusinesses = new HashMap<String, Long>();
            itemsBs = new String[mBusinesses.size()];
            int c = 0;
            for (Business mBS: mBusinesses) {
                spBusinesses.put(mBS.getBsName(),mBS.getId());
                //bsAdapter.add(mBS.getBsName());
                itemsBs[c] = mBS.getBsName();
                c++;
            }
            bsAdapter = new ArrayAdapter<String>
                    (getContext(), android.R.layout.simple_spinner_dropdown_item, itemsBs);
            bsList.setAdapter(bsAdapter);
        }
    }
}

