package com.techmata.dealpundit.bs.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * A placeholder fragment containing a simple view.
 */
public class FragmentAbout extends Fragment {

    Button btnBack;
    public FragmentAbout() {
    }
    FragmentManager mFragManager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_about, container, false);
        mFragManager = getFragmentManager();
        ((MainActivity) getActivity()).acBar.setTitle("About DealPundit");

        TextView tvTerms = (TextView) v.findViewById(R.id.tvDesigned);
        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TermsActivity.class);
                startActivity(intent);
            }
        });
        btnBack =(Button) v.findViewById(R.id.btnAboutBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragManager.popBackStack();
            }
        });

        return v;
    }
}
