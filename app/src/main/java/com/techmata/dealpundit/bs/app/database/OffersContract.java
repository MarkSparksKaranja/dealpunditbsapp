package com.techmata.dealpundit.bs.app.database;

import android.provider.BaseColumns;

/**
 * Created by SparkSoft on 18/03/2015.
 */
public class OffersContract {
    /* Inner class that defines the table contents */
    public static abstract class UserDetails implements BaseColumns {
        public static final String TABLE_NAME = "user_details";
        public static final String COLUMN_NAME_DETAIL_ID = "detail_id";
        public static final String COLUMN_NAME_DETAIL_NAME = "detail_name";
        public static final String COLUMN_NAME_DETAIL_VALUE = "detail_value";
    }
    public static abstract class Offers implements BaseColumns {
        public static final String TABLE_NAME = "offers";
        public static final String COLUMN_NAME_OFFER_ID = "offer_id";
        public static final String COLUMN_NAME_CAT_ID = "cat_id";
        public static final String COLUMN_NAME_OFFER_NAME = "offer_name";
        public static final String COLUMN_NAME_OFFER_DES = "offer_des";
        public static final String COLUMN_NAME_BS_NAME = "bs_name";
        public static final String COLUMN_NAME_BS_ID = "bs_id";
        public static final String COLUMN_NAME_IMG_DATA = "img_data";
        public static final String COLUMN_NAME_FAV = "fav";
        public static final String COLUMN_NAME_FEATURED = "featured";
    }
    public static abstract class BusinessCategories implements BaseColumns {
        public static final String TABLE_NAME = "bs_categories";
        public static final String COLUMN_NAME_BS_ID = "bs_id";
        public static final String COLUMN_NAME_CATEGORY_ID = "cat_id";
        public static final String COLUMN_NAME_CATEGORY_NAME = "cat_name";
        public static final String COLUMN_NAME_CATEGORY_DES = "cat_des";
    }
    public static abstract class Products implements BaseColumns {
        public static final String TABLE_NAME = "products";
        public static final String COLUMN_NAME_PRODUCT_ID = "product_id";
        public static final String COLUMN_NAME_BS_ID = "bs_id";
        public static final String COLUMN_NAME_CAT_ID = "cat_id";
        public static final String COLUMN_NAME_PRODUCT_NAME = "prod_name";
        public static final String COLUMN_NAME_PRODUCT_DES = "prod_des";
        public static final String COLUMN_NAME_BS_NAME = "bs_name";
        public static final String COLUMN_NAME_IMG_DATA = "img_data";
        public static final String COLUMN_NAME_PRICE = "price";
    }
    public static abstract class OffersTimes implements BaseColumns {
        public static final String TABLE_NAME = "offer_times";
        public static final String COLUMN_NAME_TIME_ID = "time_id";
        public static final String COLUMN_NAME_OFFER_ID = "offer_id";
        public static final String COLUMN_NAME_START_TIME = "start_time";
        public static final String COLUMN_NAME_START_DATE = "start_date";
        public static final String COLUMN_NAME_STOP_TIME = "stop_time";
        public static final String COLUMN_NAME_STOP_DATE = "stop_date";
    }
    public static abstract class Categories implements BaseColumns {
        public static final String TABLE_NAME = "categories";
        public static final String COLUMN_NAME_CATEGORY_ID = "cat_id";
        public static final String COLUMN_NAME_CATEGORY_NAME = "cat_name";
        public static final String COLUMN_NAME_CATEGORY_DES = "cat_des";
    }
    public static abstract class MyBusinesses implements BaseColumns {
        public static final String TABLE_NAME = "my_businesses";
        public static final String COLUMN_NAME_BUSINESS_ID = "bs_id";
        public static final String COLUMN_NAME_BUSINESS_NAME = "bs_name";
        public static final String COLUMN_NAME_CATEGORY_ID = "cat_id";
        public static final String COLUMN_NAME_DES = "cat_des";
        public static final String COLUMN_NAME_REG_DATE = "reg_date";
        public static final String COLUMN_NAME_IMG = "img_data";
    }
    public static abstract class MyBranches implements BaseColumns {
        public static final String TABLE_NAME = "my_branches";
        public static final String COLUMN_NAME_BRANCH_ID = "branch_id";
        public static final String COLUMN_NAME_BUSINESS_ID = "bs_id";
        public static final String COLUMN_NAME_BRANCH_NAME = "branch_name";
        public static final String COLUMN_NAME_LOC_NAME = "loc_name";
        public static final String COLUMN_NAME_LAT = "branch_lat";
        public static final String COLUMN_NAME_LON = "branch_lon";
    }
}
