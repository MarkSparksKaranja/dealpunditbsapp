package com.techmata.dealpundit.bs.app.adapters;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;
import com.techmata.dealpundit.bs.app.R;
import com.techmata.dealpundit.bs.app.models.OfferTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Sparks on 26/03/2016.
 */

public class OfferTimesAdapter extends RecyclerView.Adapter<OfferTimesAdapter.ViewHolderOfferTime> {

    //contains the list of posts
    private ArrayList<OfferTime> myTimes = new ArrayList<OfferTime>();
    private LayoutInflater mInflater;
    Context mcontext;
    String timeStamp;
    ViewHolderOfferTime viewHolder;
    Date date;

    public OfferTimesAdapter(Context context, ArrayList<OfferTime> myTimes) {
        this.mcontext = context;
        mInflater = LayoutInflater.from(context);
        this.myTimes = myTimes;
        myTimes.add(new OfferTime(Calendar.getInstance(),Calendar.getInstance(),Calendar.getInstance(),Calendar.getInstance()));
    }

    public void addTime(int a) {
        OfferTime acTime = new OfferTime(mcontext);
        //add everything
        this.myTimes.add(a,acTime);
        //update the adapter to reflect the new set of posts
        notifyDataSetChanged();
    }
    public ArrayList<OfferTime> getTimes(){
        //Log.i("myOfferTimes",String.valueOf(myTimes.size()));
        //Log.i("myOfferTimes",myTimes.toString());
        return myTimes;
    }

    @Override
    public ViewHolderOfferTime onCreateViewHolder(final ViewGroup parent, int viewType) {
        final View view = mInflater.inflate(R.layout.item_offer_time, parent, false);
        final ViewHolderOfferTime vHolder = new ViewHolderOfferTime(view,mcontext);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolderOfferTime holder, final int position) {

        this.viewHolder = holder;
        /*SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try {
            date = formatter.parse(currentActivity.getTimestamp().substring(0, 24));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        timeStamp = (String) DateUtils.getRelativeTimeSpanString(date.getTime(),
                System.currentTimeMillis(),
                DateUtils.SECOND_IN_MILLIS);*/
    }

    @Override
    public int getItemCount() {
        return myTimes.size();
    }

    public boolean removeAt(int position) {
        if(myTimes.size() <= 1 ){
            Toast.makeText(mcontext,"Has to be at least one",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            myTimes.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, myTimes.size());
            return true;
        }
    }

    class ViewHolderOfferTime extends RecyclerView.ViewHolder {

        ImageButton btnClose;
        TextView tvStart ,tvStop, tvtFTime, tvSTime;
        SimpleDateFormat dateFormatter,timeFormatter;
        DatePickerDialog fromDatePickerDialog,toDatePickerDialog;
        TimePickerDialog fromTimePickerDialog,toTimePickerDialog;

        public ViewHolderOfferTime(final View itemView, final Context mcontext) {
            super(itemView);
            dateFormatter = new SimpleDateFormat("ccc, dd, MMM, yyyy");
            timeFormatter = new SimpleDateFormat("hh:mm a");
            Calendar newCalendar = Calendar.getInstance();
            Calendar todayDate = Calendar.getInstance();

            btnClose = (ImageButton) itemView.findViewById(R.id.btnClose);
            btnClose.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setVisibility(View.GONE);
                    removeAt(getAdapterPosition());
                    v.setVisibility(View.VISIBLE);
                }
            });

            tvStart = (TextView) itemView.findViewById(R.id.tvStart);
            tvStart.setText(dateFormatter.format(todayDate.getTime()));
            myTimes.get(0).setfDate(todayDate);
            fromDatePickerDialog = new DatePickerDialog(mcontext,
                    new DatePickerDialog.OnDateSetListener() {
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            Calendar newDate = Calendar.getInstance();
                            newDate.set(year, monthOfYear, dayOfMonth);
                            tvStart.setText(dateFormatter.format(newDate.getTime()));
                            myTimes.get(getAdapterPosition()).setfDate(newDate);
                        }
                    }
                    ,newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            tvStart.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    fromDatePickerDialog.show();
                }
            });

            tvtFTime  = (TextView) itemView.findViewById(R.id.tvFTime);
            tvtFTime.setText(timeFormatter.format(todayDate.getTime()));
            myTimes.get(0).setfTime(todayDate);
            fromTimePickerDialog = new TimePickerDialog(mcontext, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(Calendar.HOUR,hourOfDay);
                    newDate.set(Calendar.MINUTE,minute);
                    tvtFTime.setText(timeFormatter.format(newDate.getTime()));
                    myTimes.get(getAdapterPosition()).setfTime(newDate);
                }
            },newCalendar.get(Calendar.HOUR),newCalendar.get(Calendar.MINUTE),false);
            tvtFTime.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    fromTimePickerDialog.show();
                }
            });

            tvStop = (TextView) itemView.findViewById(R.id.tvStop);
            tvStop.setText(dateFormatter.format(todayDate.getTime()));
            myTimes.get(0).settDate(todayDate);
            toDatePickerDialog = new DatePickerDialog(mcontext,
                    new DatePickerDialog.OnDateSetListener() {
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            Calendar newDate = Calendar.getInstance();
                            newDate.set(year, monthOfYear, dayOfMonth);
                            tvStop.setText(dateFormatter.format(newDate.getTime()));
                            myTimes.get(getAdapterPosition()).settDate(newDate);
                        }
                    }
                    ,newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            tvStop.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    toDatePickerDialog.show();
                }
            });



            tvSTime  = (TextView) itemView.findViewById(R.id.tvSTime);
            tvSTime.setText(timeFormatter.format(todayDate.getTime()));
            myTimes.get(0).settTime(todayDate);
            toTimePickerDialog = new TimePickerDialog(mcontext, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(Calendar.HOUR,hourOfDay);
                    newDate.set(Calendar.MINUTE,minute);
                    tvSTime.setText(timeFormatter.format(newDate.getTime()));
                    myTimes.get(getAdapterPosition()).settTime(newDate);
                }
            },newCalendar.get(Calendar.HOUR),newCalendar.get(Calendar.MINUTE),false);
            tvSTime.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    toTimePickerDialog.show();
                }
            });


        }
    }
}

