package com.techmata.dealpundit.bs.app.models;

import android.util.Base64;

import java.util.ArrayList;

/**
 * Created by Sparks on 06/10/2016.
 */
public class Business {
    long id;
    String bsName, bsDes, imgData;
    ArrayList<Branch> branches ;

    public Business(long id,String bsName, String bsDes) {
        this.id = id;
        this.bsName = bsName;
        this.bsDes = bsDes;
    }
    public Business(String bsName, String bsDes) {
        this.bsName = bsName;
        this.bsDes = bsDes;
    }
    public Business( String bsName, String bsDes, ArrayList<Branch> branches,String imgData) {
        this.bsName = bsName;
        this.bsDes = bsDes;
        this.branches = branches;
        this.imgData = imgData;
    }
    public Business(long id, String bsName, String bsDes, ArrayList<Branch> branches,String imgData) {
        this.id = id;
        this.bsName = bsName;
        this.bsDes = bsDes;
        this.branches = branches;
        this.imgData = imgData;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBsName() {
        return bsName;
    }

    public void setBsName(String bsName) {
        this.bsName = bsName;
    }

    public String getBsDes() {
        return bsDes;
    }

    public void setBsDes(String bsDes) {
        this.bsDes = bsDes;
    }

    public ArrayList<Branch> getBranches() {
        return branches;
    }

    public void setBranches(ArrayList<Branch> branches) {
        this.branches = branches;
    }

    public String getImgData() {
        return imgData;
    }
    public byte[] getImg(){
       return Base64.decode(imgData, Base64.DEFAULT);
    }

    public void setImgData(String imgData) {
        this.imgData = imgData;
    }
}
