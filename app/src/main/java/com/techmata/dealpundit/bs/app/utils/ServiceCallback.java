package com.techmata.dealpundit.bs.app.utils;

/**
 * Created by Sparks on 01/10/2016.
 */
public interface ServiceCallback {
    void onSuccess();
}
