package com.techmata.dealpundit.bs.app;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

/**
 * Created by Sparks on 28/03/2016.
 */
public class TermsActivity extends AppCompatActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_terms);

        ActionBar tlb = getSupportActionBar();
        tlb.setDisplayHomeAsUpEnabled(true);
        tlb.setHomeButtonEnabled(true);
        tlb.setTitle("TERMS AND CONDITIONS");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
